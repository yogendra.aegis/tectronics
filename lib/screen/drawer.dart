import 'dart:io';

import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/const.dart';
import 'package:tectronics/screen/product/productsearch.dart';
import 'package:tectronics/screen/profile/profile.dart';
import 'package:url_launcher/url_launcher.dart';
import '../auth.dart';
import './product/products.dart';
import './home/home.dart';

import './aboutus.dart';
import './contactus.dart';
import './subscribe.dart';
import 'download/download.dart';
import 'download/download_pdf.dart';
import 'gallery/gallery.dart';

class DrawerOnly extends StatelessWidget {
  final User user;
  DrawerOnly({Key key, @required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int _currentSelected = 0;

    double width = MediaQuery.of(context).size.width;
    return SizedBox(
      width: width / 1.25,
      child: new Drawer(
          child: Container(
        color: textColor,
        child: new ListView(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            // new DrawerHeader(
            //   child: new Image.asset('assets/menu/logo-menu.png',
            //       height: 50.0, width: 358.0),
            //   decoration: new BoxDecoration(color: menuBG, border: Border()),
            // ),

            Container(
                height: 80.0,
                child: Center(
                  child: new Image.asset('assets/menu/logo-menu.png',
                      height: 50.0, width: 358.0),
                )),

            SizedBox(
              height: 20,
            ),

            Container(
              height: 50.0,
              child: new ListTile(
                  dense: true,
                  title: new Text(
                    (user != null) ? user.name : "No name",
                    style: TextStyle(
                        color: whiteColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0),
                  ),
                  leading: new Icon(Icons.account_circle, color: whiteColor),
                  selected: true,
                  onTap: () {
                    _currentSelected = 0;
                    Navigator.of(context).pop();
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (ctxt) => new ProfilePage(user: user)));
                  }),
            ),
            new Divider(
              color: whiteColor,
            ),
            // Container(
            //   height: 40.0,
            // child:
            new ListTile(
                dense: true,
                title: new Text(
                  "Home",
                  style: TextStyle(
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0),
                ),
                leading: new Icon(Icons.home, color: whiteColor),
                selected: true,
                onTap: () {
                  _currentSelected = 1;
                  Navigator.of(context).pop();
                  Navigator.push(context,
                      new MaterialPageRoute(builder: (ctxt) => new HomePage()));
                }),
            // ),

            new Divider(
              color: whiteColor,
            ),
            new ListTile(
                dense: true,
                title: new Text(
                  "About Us",
                  style: TextStyle(
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0),
                ),
                leading: new Icon(Icons.explore, color: whiteColor),
                onTap: () {
                  _currentSelected = 3;
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (ctxt) => new AboutUSPage(
                                user: user,
                              )));
                }),
            new Divider(
              color: whiteColor,
            ),
            new ListTile(
                dense: true,
                title: new Text(
                  "Our Products",
                  style: TextStyle(
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0),
                ),
                leading: new Icon(Icons.category, color: whiteColor),
                onTap: () {
                  _currentSelected = 2;
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (ctxt) => new ProductsPage(user: user)));
                }),
            new Divider(
              color: whiteColor,
            ),
            new ListTile(
                dense: true,
                title: new Text(
                  "Product Selector",
                  style: TextStyle(
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0),
                ),
                leading: new Icon(Icons.filter_list, color: whiteColor),
                onTap: () {
                  _currentSelected = 2;
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (ctxt) =>
                              new ProductSearchPage(user: user)));
                }),

            new Divider(
              color: whiteColor,
            ),
            new ListTile(
                dense: true,
                title: new Text(
                  "Gallery",
                  style: TextStyle(
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0),
                ),
                leading: new Icon(Icons.photo_album, color: whiteColor),
                onTap: () {
                  _currentSelected = 3;
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (ctxt) => new GalleryViewPage(
                                user: user,
                              )));
                }),

            new Divider(
              color: whiteColor,
            ),

            // new ListTile(
            //     title: new Text(
            //       "Download",
            //       style: TextStyle(
            //           color: whiteColor,
            //           fontWeight: FontWeight.bold,
            //           fontSize: 18.0),
            //     ),
            //     leading: new Icon(Icons.file_download, color: whiteColor),
            //     onTap: () {
            //       _currentSelected = 3;
            //       Navigator.of(context).pop();
            //       Navigator.push(
            //           context,
            //           new MaterialPageRoute(
            //               builder: (ctxt) => new DownloadPage(
            //                     user: user,
            //                   )));
            //     }),

            // new Divider(
            //   color: whiteColor,
            // ),

            new ListTile(
                dense: true,
                title: new Text(
                  "Downloads",
                  style: TextStyle(
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0),
                ),
                leading: new Icon(Icons.file_download, color: whiteColor),
                onTap: () {
                  _currentSelected = 3;
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (ctxt) => new DownloadPdfPage(
                                user: user,
                              )));
                }),

            new Divider(
              color: whiteColor,
            ),

            new ListTile(
                dense: true,
                title: new Text(
                  "Contact Us",
                  style: TextStyle(
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0),
                ),
                leading: new Icon(Icons.rate_review, color: whiteColor),
                onTap: () {
                  _currentSelected = 3;
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (ctxt) => new ContactUSPage(
                                user: user,
                              )));
                }),
            new Divider(
              color: whiteColor,
            ),

            // new ListTile(
            //     title: new Text(
            //       "Tollfree Number",
            //       style: TextStyle(
            //           color: whiteColor,
            //           fontWeight: FontWeight.bold,
            //           fontSize: 18.0),
            //     ),
            //     subtitle: new Text(
            //       "1800 1212 312",
            //       style: TextStyle(
            //           color: whiteColor,
            //           fontWeight: FontWeight.bold,
            //           fontSize: 15.0),
            //     ),
            //     leading: new Icon(Icons.phone, color: whiteColor),
            //     onTap: () {
            //       Navigator.of(context).pop();
            //       call("18001212312");
            //     }),
            // new Divider(
            //   color: whiteColor,
            // ),

            new ListTile(
                dense: true,
                title: new Text(
                  "Share App",
                  style: TextStyle(
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0),
                ),
                leading: new Icon(Icons.share, color: whiteColor),
                onTap: () {
                  if (Platform.isAndroid) {
                    Share.share(
                        'https://play.google.com/store/apps/details?id=com.tectronics');
                  } else {
                    Share.share('https://apps.apple.com/app/id1497089802');
                  }
                }),
            new Divider(
              color: whiteColor,
            ),

            new ListTile(
                dense: true,
                title: new Text(
                  "Logout",
                  style: TextStyle(
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0),
                ),
                leading: new Icon(Icons.verified_user, color: whiteColor),
                onTap: () {
                  _currentSelected = 4;
                  var db = new DatabaseHelper();
                  db.deleteUser(user);
                  Navigator.of(context).pushReplacementNamed("/login");
                  var authStateProvider = new AuthStateProvider();
                  // authStateProvider.dispose;
                  authStateProvider.notify(AuthState.LOGGED_OUT);
                  // Navigator.of(context).pop();
                }),

            new Divider(
              color: whiteColor,
            ),

            new SizedBox(
                height: 200,
                child: Center(
                  child: Column(
                    children: <Widget>[
                      new Text(
                        "Follow US",
                        style: TextStyle(
                            color: whiteColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0),
                      ),
                      // SizedBox(height: 10),
                      ButtonBar(
                        mainAxisSize: MainAxisSize
                            .min, // this will take space as minimum as posible(to center)

                        children: <Widget>[
                          SizedBox(
                            height: 50,
                            width: 50,
                            child: InkWell(
                              splashColor: textColor, // splash color
                              onTap: () {
                                Navigator.of(context).pop();
                                _launchInBrowser(
                                    "https://www.facebook.com/tectronicsengineers");
                              }, // button pressed
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Image.asset(
                                    "assets/followus/fb.png",
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            width: 50,
                            child: InkWell(
                              splashColor: textColor, // splash color
                              onTap: () {
                                Navigator.of(context).pop();
                                _launchInBrowser(
                                    "https://twitter.com/tectronicsindia");
                              }, // button pressed
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Image.asset(
                                    "assets/followus/tw.png",
                                  ),
                                  // Text("Call"), // text
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            width: 50,
                            child: InkWell(
                              splashColor: textColor, // splash color
                              onTap: () {
                                Navigator.of(context).pop();
                                _launchInBrowser(
                                    "https://www.youtube.com/user/tectronicsengineers");
                              }, // button pressed
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Image.asset(
                                    "assets/followus/yo.png",
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 2.5),

                      FlatButton.icon(
                          color: textColor,
                          onPressed: () {
                            Navigator.of(context).pop();
                            call("18001212312");
                          },
                          icon: new Icon(Icons.phone, color: whiteColor),
                          label: new Text(
                            "1800 1212 312",
                            style: TextStyle(
                                color: whiteColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0),
                          )),

                      SizedBox(height: 10),

                      Text(
                        (Platform.isAndroid)
                            ? "Version : 1.0.6"
                            : "Version : 1.0.5",
                        style: TextStyle(
                            color: whiteColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0),
                      ),
                    ],
                  ),
                )),
          ],
        ),
      )),
    );
  }

  void call(String number) async {
    var url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }
}
