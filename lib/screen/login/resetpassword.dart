import 'dart:ui';
import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/const.dart';
import 'package:tectronics/screen/login/otpverify.dart';

class ResetPasswordPage extends StatefulWidget {
  ResetPasswordPage({Key key, this.userId}) : super(key: key);
  final userId;
  @override
  ResetPasswordState createState() => new ResetPasswordState(userId);
}

class ResetPasswordState extends State<ResetPasswordPage> {
  ResetPasswordState(this.userId);
  var userId;

  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();

  String _newPassword, _confirmPassword;
  bool _autoValidate = false;
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _isLoading = false;
    _newPassword = _confirmPassword = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        elevation: 10.0,
        backgroundColor: redColor,
        iconTheme: IconThemeData(
          color: Colors.white,
          //change your color here
        ),
        title: Text(
          "RESET PASSWORD",
          style: TextStyle(
            color: whiteColor,
          ),
        ),
      ),
      key: scaffoldKey,
      body: new SafeArea(
        top: false,
        bottom: false,
        child: new Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Center(
            child: !_isLoading
                ? ListView(
                    shrinkWrap: false,
                    padding: EdgeInsets.only(left: 30.0, right: 30.0),
                    children: <Widget>[
                      SizedBox(height: 20.0),
                      TextFormField(
                        obscureText: true,
                        keyboardType: TextInputType.text,
                        initialValue: _newPassword,
                        onChanged: (text) {
                          _newPassword = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'New Password',
                          labelText: 'New Password',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.lock,
                            color: redColor,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: Colors.white,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          if (value.length < 3) {
                            return ('Please enter valid New Password.');
                          }
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(40),
                        ],
                        onSaved: (String val) {
                          _newPassword = val;
                        },
                      ),
                      SizedBox(height: 10.0),
                      TextFormField(
                        obscureText: true,
                        keyboardType: TextInputType.text,
                        initialValue: _confirmPassword,
                        onChanged: (text) {
                          _confirmPassword = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'Confirm Password',
                          labelText: 'Confirm Password',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.lock,
                            color: redColor,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: Colors.white,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          if (value.length < 3) {
                            return ('Please enter valid Confirm Password.');
                          } else if (_newPassword != _confirmPassword) {
                            return ('New Password and Confirm Password must be same.');
                          }
                          // return null;
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(40),
                        ],
                        onSaved: (String val) {
                          _confirmPassword = val;
                        },
                      ),
                      SizedBox(height: 10.0),
                      SizedBox(
                        height: 50.0, // match_parent
                        child: RaisedButton(
                          onPressed: _validateInputs,
                          // padding: EdgeInsets.all(10),
                          color: redColor,
                          child: Text(
                            "SUBMIT",
                            style: TextStyle(
                              color: whiteColor,
                              fontSize: 22.0,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20.0),
                    ],
                  )
                : new Stack(
                    children: [
                      new Opacity(
                        opacity: 0.3,
                        child: const ModalBarrier(
                            dismissible: false, color: Colors.grey),
                      ),
                      new Center(
                        child: new CircularProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(redColor),
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  void _validateInputs() {
    final _form = _formKey.currentState;
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      setState(() => _isLoading = true);
      _form.save();
      doResetPassword();
      // If the form is valid, we want to show a Snackbar

    } else {
      _autoValidate = true;
    }
  }

  void doResetPassword() async {
    RestData api = new RestData();
    try {
      var request =
          await api.resetPassword(userId.toString(), _newPassword.toString());
      if (request["Response"] == true) {
        _showSnackBar("Updated Password Successfully!");
        Future.delayed(Duration(seconds: 1), () {
          Navigator.pop(context, true);
        });
      } else {
        _showSnackBar(request["Message"].toString());
      }
      setState(() => _isLoading = false);
    } catch (error) {
      _showSnackBar(error.toString());
    }
  }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
}
