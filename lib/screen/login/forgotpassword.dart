import 'dart:ui';
import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/const.dart';
import 'package:tectronics/screen/login/otpverify.dart';
import 'package:tectronics/screen/login/resetpassword.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  ForgotPasswordState createState() => ForgotPasswordState();
}

class ForgotPasswordState extends State<ForgotPasswordPage> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();

  String _userName;
  bool _autoValidate = false;
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _isLoading = false;
    _userName = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        elevation: 10.0,
        backgroundColor: redColor,
        iconTheme: IconThemeData(
          color: Colors.white,
          //change your color here
        ),
        title: Text(
          "FORGOT PASSWORD",
          style: TextStyle(
            color: whiteColor,
          ),
        ),
      ),
      key: scaffoldKey,
      body: new SafeArea(
        top: false,
        bottom: false,
        child: new Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Center(
            child: !_isLoading
                ? ListView(
                    shrinkWrap: false,
                    padding: EdgeInsets.only(left: 30.0, right: 30.0),
                    children: <Widget>[
                      SizedBox(height: 20.0),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        initialValue: _userName,
                        onChanged: (text) {
                          _userName = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'Email / Mobile',
                          labelText: 'Email / Mobile',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.person,
                            color: Colors.black,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: formElementColor,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          String pattternEmail =
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";

                          String pattternMobile = r'(^(?:[+0]9)?[0-9]{10,12}$)';

                          RegExp regExpE = new RegExp(pattternEmail);
                          RegExp regExpM = new RegExp(pattternMobile);
                          if (value.length == 0) {
                            return 'Please enter valid details';
                          } else if (regExpE.hasMatch(value)) {
                            return null;
                          } else if (regExpM.hasMatch(value)) {
                            return null;
                          }
                          return 'Please enter valid Email or Mobile';
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(40),
                        ],
                        onSaved: (String val) {
                          _userName = val;
                        },
                      ),
                      SizedBox(height: 20.0),
                      SizedBox(
                        height: 50.0, // match_parent
                        child: RaisedButton(
                          onPressed: _validateInputs,
                          // padding: EdgeInsets.all(10),
                          color: redColor,
                          child: Text(
                            "SUBMIT",
                            style: TextStyle(
                              color: whiteColor,
                              fontSize: 22.0,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 20.0),
                    ],
                  )
                : new Stack(
                    children: [
                      new Opacity(
                        opacity: 0.3,
                        child: const ModalBarrier(
                            dismissible: false, color: Colors.grey),
                      ),
                      new Center(
                        child: new CircularProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(redColor),
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  void _validateInputs() {
    final _form = _formKey.currentState;
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      // If the form is valid, we want to show a Snackbar
      setState(() => _isLoading = true);
      _form.save();
      doForgostPassword();
    } else {
      _autoValidate = true;
    }
  }

  void doForgostPassword() async {
    RestData api = new RestData();
    try {
      var request = await api.forgotpassword(
        _userName.toString(),
      );

      if (request["Response"] == true) {
        _showSnackBar(
            "We need to verify your phone number before set a new password.");
        Navigator.of(context)
            .push(
              MaterialPageRoute(
                  builder: (context) => OtpPage(
                      userId: request["Result"]["Id".toString()],
                      mobileNumber:
                          request["Result"]["MobileNumber"].toString(),
                      otpCode: request["Result"]["OTP".toString()])),
            )
            .then((val) => val
                ? Navigator.of(context)
                    .push(
                      MaterialPageRoute(
                          builder: (context) => ResetPasswordPage(
                                userId: request["Result"]["Id".toString()],
                              )),
                    )
                    .then((val) => val ? Navigator.pop(context, true) : null)
                : null);
      } else {
        _showSnackBar(request["Message"].toString());

        print("view");
      }
      setState(() => _isLoading = false);
    } catch (error) {
      _showSnackBar(error.toString());
    }
  }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
}
