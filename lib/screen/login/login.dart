import 'dart:ui';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tectronics/auth.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/screen/login/otpverify.dart';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:tectronics/const.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  String _password, _username, _token;
  bool _autoValidate = false;
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    if (Platform.isIOS) {
      _firebaseMessaging.requestNotificationPermissions(
          IosNotificationSettings(sound: true, badge: true, alert: true));
      _firebaseMessaging.onIosSettingsRegistered
          .listen((IosNotificationSettings settings) {
        print("Settings registered: $settings");
      });
    }
    
    _firebaseMessaging.getToken().then((token) {
      _token = token;
      print("Device TOken:" + token);
    });
  }

  @override
  Widget build(BuildContext context) {
    // double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: whiteColor,
      appBar: null,
      key: scaffoldKey,
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
            key: _formKey,
            autovalidate: _autoValidate,
            child: Center(
              child: !_isLoading
                  ? ListView(
                      shrinkWrap: false,
                      padding: EdgeInsets.only(left: 30.0, right: 30.0),
                      children: <Widget>[
                        // logo,
                        SizedBox(height: 20),

                        Column(
                          children: <Widget>[
                            new Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              child: new Image.asset(
                                "assets/logo.png",
                                fit: BoxFit.contain,
                                height: 180.0,
                                width: 180.0,
                              ),
                            ),
                            SizedBox(height: 20.0),
                            Text(
                              "SIGN IN",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: redColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 25.0,
                              ),
                            ),
                          ],
                        ),

                        SizedBox(height: 25.0),

                        TextFormField(
                          keyboardType: TextInputType.text,
                          initialValue: _username,
                          onChanged: (text) {
                            _username = text;
                          },
                          decoration: InputDecoration(
                            hintText: 'Email / Mobile',
                            labelText: 'Email / Mobile',
                            filled: true,
                            fillColor: formElementColor,
                            prefixIcon: Icon(
                              Icons.email,
                              color: redColor,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                              borderSide: new BorderSide(
                                color: formElementColor,
                                width: 2.0,
                              ),
                            ),
                            contentPadding:
                                EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                          ),
                          validator: (value) {
                            String pattternEmail =
                                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";

                            String pattternMobile =
                                r'(^(?:[+0]9)?[0-9]{10,12}$)';

                            RegExp regExpE = new RegExp(pattternEmail);
                            RegExp regExpM = new RegExp(pattternMobile);
                            if (value.length == 0) {
                              return 'Please enter valid details';
                            } else if (regExpE.hasMatch(value)) {
                              return null;
                            } else if (regExpM.hasMatch(value)) {
                              return null;
                            }
                            return 'Please enter valid Email or Mobile';
                          },
                          inputFormatters: [
                            new LengthLimitingTextInputFormatter(40),
                          ],
                          onSaved: (String val) {
                            _username = val;
                          },
                        ),
                        SizedBox(height: 10.0),

                        TextFormField(
                          obscureText: true,
                          keyboardType: TextInputType.text,
                          initialValue: _password,
                          onChanged: (text) {
                            _password = text;
                          },
                          decoration: InputDecoration(
                            hintText: 'Password',
                            labelText: 'Password',
                            filled: true,
                            fillColor: formElementColor,
                            prefixIcon: Icon(
                              Icons.lock,
                              color: redColor,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                              borderSide: new BorderSide(
                                color: Colors.white,
                                width: 2.0,
                              ),
                            ),
                            contentPadding:
                                EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                          ),
                          validator: (value) {
                            if (value.length < 3) {
                              return ('Please enter valid Password.');
                            }
                          },
                          inputFormatters: [
                            new LengthLimitingTextInputFormatter(40),
                          ],
                          onSaved: (String val) {
                            _password = val;
                          },
                        ),

                        SizedBox(height: 20.0),

                        SizedBox(
                            height: 50.0, // match_parent
                            child: RaisedButton(
                              onPressed: _validateInputs,
                              // padding: EdgeInsets.all(10),
                              color: redColor,
                              child: Text(
                                "SIGN IN",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22.0,
                                ),
                              ),
                            )),

                        SizedBox(height: 10.0),

                        Row(children: <Widget>[
                          Expanded(
                            child: FlatButton(
                              child: Text(
                                'Create an account?',
                                style:
                                    TextStyle(color: redColor, fontSize: 16.0),
                              ),
                              onPressed: () {
                                Navigator.of(context).pushNamed('/register');
                              },
                            ),
                          ),
                          Expanded(
                            child: FlatButton(
                              child: Text(
                                'Forgot Password?',
                                style:
                                    TextStyle(color: redColor, fontSize: 16.0),
                              ),
                              onPressed: () {
                                Navigator.of(context)
                                    .pushNamed('/forgotpassword');
                              },
                            ),
                          ),
                        ])
                      ],
                    )
                  : new Stack(
                      children: [
                        new Opacity(
                          opacity: 0.3,
                          child: const ModalBarrier(
                              dismissible: false, color: Colors.grey),
                        ),
                        new Center(
                          child: new CircularProgressIndicator(
                            valueColor:
                                new AlwaysStoppedAnimation<Color>(redColor),
                          ),
                        ),
                      ],
                    ),
            ),
          )),
    );
  }

  @override
  void onAuthStateChanged(AuthState state) {
    if (state == AuthState.LOGGED_IN)
      Navigator.of(context).pushReplacementNamed("/home");
  }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  void _validateInputs() {
    final _form = _formKey.currentState;
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      setState(() => _isLoading = true);
      _form.save();
      _doLogin();
    } else {
      _autoValidate = true;
    }
  }

  Future _doLogin() async {
    RestData api = new RestData();

    // _gender =
    try {
      var request = await api.login(
          _username.toString(), _password.toString(), _token.trim());

      if (request["Response"] == true) {
        setState(() => _isLoading = false);
        if (request["Result"]["IsVerify".toString()] == true) {
          gotoHome(request);
        } else {
          Navigator.of(context)
              .push(
                MaterialPageRoute(
                    builder: (context) => OtpPage(
                        userId: request["Result"]["Id"].toString(),
                        mobileNumber:
                            request["Result"]["MobileNumber"].toString(),
                        otpCode: request["Result"]["OTP"].toString())),
              )
              .then((val) => val ? gotoHome(request) : null);
        }
        // var test = new User.fromJson(request["Result"]);
        // var db = new DatabaseHelper();
        // await db.saveUser(test);
        // _showSnackBar("Login Successfully!!!");
      } else {
        _showSnackBar("Invalid Credentials!");
      }
      setState(() => _isLoading = false);
    } catch (error) {
      _showSnackBar(error.toString());
    }
  }

  Future gotoHome(request) async {
    var test = new User.fromJson(request["Result"]);
    print("test1");
    var db = new DatabaseHelper();

    try {
      await db.saveUser(test);
    } catch (error) {
      print(error.toString());
      await db.saveUserForce(test);
    }

    print("test2");
    _showSnackBar("Login Successfully!!!");
    Navigator.of(context).pushReplacementNamed("/home");
    var authStateProvider = new AuthStateProvider();
    authStateProvider.notify(AuthState.LOGGED_IN);
  }
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
  }

  // Or do other work.
}
