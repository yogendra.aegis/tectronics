import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/const.dart';
import 'package:url_launcher/url_launcher.dart';

import 'drawer.dart';

class ContactUSPage extends StatefulWidget {
  ContactUSPage({Key key, this.user}) : super(key: key);
  final User user;

  @override
  ContactUSState createState() => ContactUSState(user);
}

class ContactUSState extends State<ContactUSPage> {
  User user;
  ContactUSState(this.user);

  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();

  String _fullName, _mobileNumber, _email, _description, _dropdown;
  bool _autoValidate = false;
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  DateTime selectedDate = DateTime.now();

  List<String> _dataDropDown = ['Sales', 'Service/Support']; // Option 2
  String _selectedData;

  @override
  void initState() {
    super.initState();
    _selectedData = _dataDropDown[0];
    _isLoading = false;
    _fullName = user.name;
    _mobileNumber = user.mobileNumber;
    _email = user.email;
    _description = "";
    _dropdown = "1";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      drawer: DrawerOnly(user: user),
      appBar: AppBar(
        // leading: IconButton(
        //   icon: Icon(Icons.arrow_back),
        //   onPressed: () {
        //     Navigator.pop(context);
        //   },
        // ),
        centerTitle: true,
        elevation: 10.0,
        backgroundColor: redColor,
        iconTheme: IconThemeData(
          color: Colors.white,
          //change your color here
        ),
        title: Text(
          "CONTACT US",
          style: TextStyle(
            color: whiteColor,
          ),
        ),
        // actions: <Widget>[
        //   new IconButton(
        //       icon: new Image.asset("assets/menu/menu-btn.png",
        //           height: 30.0, width: 30.0),
        //       onPressed: () {
        //         Scaffold.of(context).openEndDrawer();
        //       })
        // ],
      ),
      key: scaffoldKey,
      body: new SafeArea(
        top: false,
        bottom: false,
        child: new Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Center(
            child: !_isLoading
                ? ListView(
                    shrinkWrap: false,
                    padding: EdgeInsets.only(left: 30.0, right: 30.0),
                    children: <Widget>[
                      SizedBox(height: 20.0),
                      TextFormField(
                        keyboardType: TextInputType.text,
                        initialValue: _fullName,
                        onChanged: (text) {
                          _fullName = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'Full Name',
                          labelText: 'Full Name',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.person,
                            color: Colors.black,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: formElementColor,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          if (value.length < 3) {
                            return ('Please enter valid Full Name.');
                          }
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(40),
                        ],
                        onSaved: (String val) {
                          _fullName = val;
                        },
                      ),
                      SizedBox(height: 10.0),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        initialValue: _mobileNumber,
                        onChanged: (text) {
                          _mobileNumber = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'Mobile Number',
                          labelText: 'Mobile Number',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.dialpad,
                            color: Colors.black,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: formElementColor,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
                          RegExp regExp = new RegExp(patttern);
                          if (value.length == 0) {
                            return 'Please enter mobile number';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Please enter valid mobile number';
                          }
                          return null;
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(40),
                        ],
                        onSaved: (String val) {
                          _mobileNumber = val;
                        },
                      ),
                      SizedBox(height: 10.0),
                      TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        initialValue: _email,
                        onChanged: (text) {
                          _email = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'Email',
                          labelText: 'Email',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.email,
                            color: Colors.black,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: formElementColor,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          String patttern =
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
                          RegExp regExp = new RegExp(patttern);
                          if (value.length == 0) {
                            return 'Please enter Email Address';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Please enter valid Email Address';
                          }
                          return null;
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(40),
                        ],
                        onSaved: (String val) {
                          _email = val;
                        },
                      ),
                      SizedBox(height: 10.0),
                      new FormField(
                          // validator: (value) {
                          //   if (value == null) {
                          //     return "Select Inquiry Type";
                          //   } else if (value == "Select Inquiry Type") {
                          //     return "Please Select Inquiry Type";
                          //   }
                          //   return null;
                          // },

                          builder: (FormFieldState state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            hintText: 'Select Inquiry Type',
                            labelText: 'Select Inquiry Type',
                            filled: true,
                            fillColor: formElementColor,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                              borderSide: new BorderSide(
                                color: formElementColor,
                                width: 2.0,
                              ),
                            ),
                            contentPadding:
                                EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                          ),
                          child: new DropdownButtonHideUnderline(
                            child: new DropdownButton(
                              value: _selectedData,
                              isDense: true,
                              onChanged: (newValue) {
                                setState(() {
                                  _dropdown = (newValue == "Sales") ? "1" : "2";
                                  _selectedData = newValue;
                                });
                              },
                              items: _dataDropDown.map((value) {
                                return DropdownMenuItem(
                                  child: new Text(value),
                                  value: value,
                                );
                              }).toList(),
                            ),
                          ),
                        );
                      }),
                      SizedBox(height: 10.0),
                      TextFormField(
                        keyboardType: TextInputType.multiline,
                        maxLines: 5,
                        initialValue: _description,
                        onChanged: (text) {
                          _description = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'Description',
                          labelText: 'Description',
                          filled: true,
                          fillColor: formElementColor,
                          // prefixIcon: Icon(
                          //   Icons.person,
                          //   color: Colors.black,
                          // ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: formElementColor,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          if (value.length < 3) {
                            return ('Please enter valid details.');
                          }
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(200),
                        ],
                        onSaved: (String val) {
                          _description = val;
                        },
                      ),
                      SizedBox(height: 20.0),
                      SizedBox(
                        height: 50.0, // match_parent
                        child: RaisedButton(
                          onPressed: _validateInputs,
                          // padding: EdgeInsets.all(10),
                          color: redColor,
                          child: Text(
                            "SUBMIT",
                            style: TextStyle(
                              color: whiteColor,
                              fontSize: 22.0,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 35.0),
                      Text(
                        "Registered Office",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 23, fontWeight: FontWeight.w900),
                      ),
                      SizedBox(height: 5),
                      Text(
                        "105, Royal Complex, Dhebar Road, Rajkot-360002, (Gujarat) INDIA.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(height: 20.0),
                      Text(
                        "Plant",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 23, fontWeight: FontWeight.w900),
                      ),
                      SizedBox(height: 5),
                      Text(
                        "PLOT NO: 59 to 62, Jaynath Industrial Area-3,Survey No: 148/1, Kothariya Village Main Road, Village: LOTHDA - 360022, District: RAJKOT",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(height: 20.0),
                      GestureDetector(
                        onTap: () {
                          call("18001212312");
                        },
                        child: new Row(
                          // mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.call,
                              color: Colors.red,
                              size: 35.0,
                            ),
                            new SizedBox(
                              width: 5.0,
                            ),
                            Text(
                              "1800 1212 312",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      GestureDetector(
                        onTap: () {
                          call("+919099071747");
                        },
                        child: new Row(
                          // mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.call,
                              color: Colors.red,
                              size: 35.0,
                            ),
                            new SizedBox(
                              width: 5.0,
                            ),
                            Text(
                              "+919099071747",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 20.0),
                      GestureDetector(
                        onTap: () {
                          sendEmail("info@tectronicsindia.com");
                        },
                        child: new Row(
                          // mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.mail,
                              color: Colors.red,
                              size: 35.0,
                            ),
                            new SizedBox(
                              width: 5.0,
                            ),
                            Text(
                              "info@tectronicsindia.com",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 30.0),
                    ],
                  )
                : new Stack(
                    children: [
                      new Opacity(
                        opacity: 0.3,
                        child: const ModalBarrier(
                            dismissible: false, color: Colors.grey),
                      ),
                      new Center(
                        child: new CircularProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(redColor),
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  void _validateInputs() {
    print("object");
    final _form = _formKey.currentState;
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      setState(() => _isLoading = true);
      _form.save();
      doContactUS();
    } else {
      _autoValidate = true;
    }
  }

  // void _validateInputs() {
  //   final _form = _formKey.currentState;

  //   FocusScope.of(context).requestFocus(new FocusNode());
  //   if (_formKey.currentState.validate()) {
  //     // If the form is valid, we want to show a Snackbar
  //     setState(() => _isLoading = true);
  //     _form.save();
  //     doContactUS();
  //   } else {
  //     _autoValidate = true;
  //   }
  // }

  void doContactUS() async {
    RestData api = new RestData();
    try {
      var request = await api.contactUS(
        user.id.toString(),
        _fullName.toString(),
        _email.toString(),
        _mobileNumber.toString(),
        _description.toString(),
        _dropdown.toString(),
      );

      if (request["Response"] == true) {
        _showSnackBar("Your form is submitted successfully!");
      } else {
        _showSnackBar(request["Message"].toString());
        print("view");
      }
      setState(() => _isLoading = false);
    } catch (error) {
      _showSnackBar(error.toString());
    }
  }

  Future showAlert(BuildContext context, String title) async {
    await showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Text(title),
              actions: <Widget>[
                new FlatButton(
                  child: new Text("Dismiss"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            )).then((val) {
      Navigator.pop(context);
    });
  }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  void call(String number) async {
    var url = "tel:$number";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void sendEmail(String email) async {
    var url = "mailto:$email";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
