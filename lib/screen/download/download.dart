import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/const.dart';

import '../drawer.dart';
import 'downloaddetails.dart';

class DownloadPage extends StatefulWidget {
  DownloadPage({Key key, this.user}) : super(key: key);
  final User user;

  @override
  DownloadState createState() => DownloadState(user);
}

class DownloadState extends State<DownloadPage> {
  User user;
  DownloadState(this.user);

  bool _isLoading = false;

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  List galleryListDataEvent = [];

  // String _selectedData;

  @override
  void initState() {
    super.initState();
    _isLoading = false;
    _getUserData();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: whiteColor,
        drawer: DrawerOnly(user: user),
        appBar: AppBar(
          centerTitle: true,
          elevation: 10.0,
          backgroundColor: redColor,
          iconTheme: IconThemeData(
            color: Colors.white,
            //change your color here
          ),
          title: Text(
            "Download",
            style: TextStyle(
              color: whiteColor,
            ),
          ),
        ),
        key: scaffoldKey,
        body: new SafeArea(
            top: false,
            bottom: false,
            child: new Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              // height: 200.0,
              child: !_isLoading
                  ? ListView(
                      shrinkWrap: false,
                      padding: EdgeInsets.only(left: 20.0, right: 20.0),
                      children: <Widget>[
                          SizedBox(height: 20.0),
                          SizedBox(
                              height: 45.0, // match_parent
                              child: RaisedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => DownloadDetailsPage(
                                            selectedURL:
                                                "https://tectronicsindia.com/UploadedImage/CompanyProfile.pdf",
                                            titleName: "Company Profile",
                                          )));
                                },
                                // padding: EdgeInsets.all(10),
                                color: redColor,
                                child: Text(
                                  "Company Profile",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 22.0,
                                  ),
                                ),
                              )),
                          SizedBox(height: 20.0),
                          SizedBox(
                              height: 45.0, // match_parent
                              child: RaisedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => DownloadDetailsPage(
                                            selectedURL:
                                                "https://tectronicsindia.com/UploadedImage/SafetyInstructions.pdf",
                                            titleName:
                                                "Safety Instructions (Do/Don’ts)",
                                          )));
                                },
                                // padding: EdgeInsets.all(10),
                                color: redColor,
                                child: Text(
                                  "Safety Instructions (Do/Don’ts)",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 22.0,
                                  ),
                                ),
                              )),
                          SizedBox(height: 20.0),
                          SizedBox(
                              height: 45.0, // match_parent
                              child: RaisedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => DownloadDetailsPage(
                                            selectedURL:
                                                "https://tectronicsindia.com/UploadedImage/WarrantyPolicy.pdf",
                                            titleName:
                                                "Product Warranty Policy",
                                          )));
                                },
                                // padding: EdgeInsets.all(10),
                                color: redColor,
                                child: Text(
                                  "Product Warranty Policy",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 22.0,
                                  ),
                                ),
                              )),
                        ])
                  : new Stack(
                      children: [
                        new Opacity(
                          opacity: 0.3,
                          child: const ModalBarrier(
                              dismissible: false, color: Colors.grey),
                        ),
                        new Center(
                          child: new CircularProgressIndicator(
                            valueColor:
                                new AlwaysStoppedAnimation<Color>(redColor),
                          ),
                        ),
                      ],
                    ),
            )));
  }

  Future _showSnackBar(String text) async {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  _getUserData() async {
    var db = new DatabaseHelper();
    User user1 = await db.getData();
    setState(() {
      user = user1;
      // _getGallerynData();
    });
  }
}
