import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
// import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import '../../const.dart';

class DownloadDetailsPage extends StatefulWidget {
  DownloadDetailsPage({Key key, this.selectedURL, this.titleName})
      : super(key: key);
  final selectedURL;
  final titleName;

  @override
  DownloadDetailsState createState() =>
      DownloadDetailsState(selectedURL, titleName);
}

class DownloadDetailsState extends State<DownloadDetailsPage> {
  DownloadDetailsState(this.selectedURL, this.titleName);
  var selectedURL;
  // PDFDocument document;

  var titleName;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = true;
  @override
  void initState() {
    super.initState();

    updateDocument();
  }

  Future updateDocument() async {
    await FlutterDownloader.initialize();

    // document = await PDFDocument.fromURL(selectedURL);

    setState(() {
      _isLoading = false;
    });
  }

  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return new Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        elevation: 10.0,
        backgroundColor: redColor,
        iconTheme: IconThemeData(
          color: Colors.white,
          //change your color here
        ),
        title: Text(
          titleName.toString(),
          style: TextStyle(
            color: whiteColor,
          ),
        ),
        actions: <Widget>[
          new IconButton(
              icon: Icon(Icons.file_download),
              onPressed: () {
                // PDFViewer(document: null);
                downloadPDF();
              })
        ],
      ),
      key: scaffoldKey,
      body: Center(
          child: _isLoading
              ? new Center(
                  child: new CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(redColor),
                  ),
                )
              : Container()
          // new RichText(
          //     text: new LinkTextSpan(url: selectedURL, text: 'Show My Pdf'),
          //   ),

          // PDFViewer(
          //     indicatorBackground: redColor,
          //     indicatorText: whiteColor,
          //     tooltip: PDFViewerTooltip(first: "test"),
          //     showNavigation: true,
          //     showPicker: false,
          //     showIndicator: true,
          //     document: document,
          //     // indicatorPosition: ,
          //   )
          ),
    );
  }

  void _showSnackBar(String text) {
    // setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  Future downloadPDF() async {
    final taskId = await FlutterDownloader.enqueue(
      fileName: titleName + ".pdf",
      url: selectedURL,
      savedDir:
          (await _findLocalPath()) + Platform.pathSeparator + 'tectronics',

      showNotification:
          true, // show download progress in status bar (for Android)
      openFileFromNotification:
          true, // click on notification to open downloaded file (for Android)
    );
  }

  Future<String> _findLocalPath() async {
    final directory = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    return directory.path;
  }

  // Future<File> _downloadFile(String url, String filename) async {
  //   http.Client client = new http.Client();
  //   var req = await _client.get(Uri.parse(url));
  //   var bytes = req.bodyBytes;
  //   String dir = (await getApplicationDocumentsDirectory()).path;
  //   File file = new File('$dir/$filename');
  //   await file.writeAsBytes(bytes);
  //   return file;
  // }

  // Future<void> shareFile() async {
  //   List<dynamic> docs = await DocumentsPicker.pickDocuments;
  //   if (docs == null || docs.isEmpty) return null;

  //   await FlutterShare.shareFile(
  //     title: 'Example share',
  //     text: 'Example share text',
  //     filePath: docs[0] as String,
  //   );
  // }

  // Future<void> shareScreenShot() async {
  //   Directory directory;
  //   if (Platform.isAndroid) {
  //     directory = await getExternalStorageDirectory();
  //   } else {
  //     directory = await getApplicationDocumentsDirectory();
  //   }
  //   final String localPath =
  //       '${directory.path}/${DateTime.now().toIso8601String()}.png';

  //   await _controller.capture(path: localPath);

  //   await Future.delayed(Duration(seconds: 1));

  //   await FlutterShare.shareFile(
  //     title: 'Compartilhar comprovante',
  //     filePath: localPath,
  //   );
  // }
}
