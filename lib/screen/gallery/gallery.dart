import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/const.dart';
import 'package:tectronics/screen/gallery/gallerydetails.dart';
import 'package:transparent_image/transparent_image.dart';

import '../drawer.dart';

class GalleryViewPage extends StatefulWidget {
  GalleryViewPage({Key key, this.user}) : super(key: key);
  final User user;

  @override
  GalleryViewState createState() => GalleryViewState(user);
}

class GalleryViewState extends State<GalleryViewPage> {
  User user;
  GalleryViewState(this.user);

  bool _isLoading = false;
  // final _formKey = GlobalKey<FormState>();

  // bool _autoValidate = false;
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  List galleryListDataEvent = [];

  List galleryListDataExhibition = [];

  // String _selectedData;

  @override
  void initState() {
    super.initState();
    _isLoading = false;
    _getUserData();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: whiteColor,
        drawer: DrawerOnly(user: user),
        appBar: AppBar(
          centerTitle: true,
          elevation: 10.0,
          backgroundColor: redColor,
          iconTheme: IconThemeData(
            color: Colors.white,
            //change your color here
          ),
          title: Text(
            "Gallery",
            style: TextStyle(
              color: whiteColor,
            ),
          ),
        ),
        key: scaffoldKey,
        body: new SafeArea(
            top: false,
            bottom: false,
            child: new Container(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              // height: 200.0,
              child: !_isLoading
                  ? ListView(
                      shrinkWrap: false,
                      padding: EdgeInsets.only(left: 5.0, right: 5.0),
                      children: <Widget>[
                          SizedBox(height: 10.0),
                          Container(
                            height: 35,
                            // color: redColor,
                            child: Center(
                              child: new Text(
                                "Events",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0,
                                ),
                                maxLines: 2,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          SizedBox(height: 10.0),
                          SizedBox(
                            height: screenSize.width / 1.75,
                            child: PageView.builder(
                              controller:
                                  PageController(viewportFraction: 0.85),
                              itemCount: galleryListDataEvent.length,
                              itemBuilder:
                                  (BuildContext context, int itemIndex) {
                                return Container(
                                    color: backgroundGrey,
                                    margin: new EdgeInsets.fromLTRB(5, 0, 5, 0),
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    GalleryDetailsPage(
                                                      selectedListData:
                                                          galleryListDataEvent[
                                                                  itemIndex]
                                                              ["GalleryImages"],
                                                      titleName:
                                                          galleryListDataEvent[
                                                                  itemIndex]
                                                              ["Name"],
                                                    )));
                                        print(galleryListDataEvent[itemIndex]);
                                      },
                                      child: new GridTile(
                                          footer: new Center(
                                            child: Container(
                                              color: backgroundGrey,
                                              child: new Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 3.0,
                                                    left: 2.0,
                                                    right: 2.0),
                                                child: new AutoSizeText(
                                                  '${galleryListDataEvent[itemIndex]["Name"]}',
                                                  style: new TextStyle(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.bold,
                                                    color: redColor,
                                                  ),
                                                  maxLines: 1,
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            ),
                                          ),

                                          // new Text("data[index]['name']"),
                                          child: new Center(
                                            child: new Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 0.0,
                                                    bottom: 30.0,
                                                    right: 0.0,
                                                    top: 0.0),
                                                child: Stack(
                                                  children: <Widget>[
                                                    Center(
                                                      child:
                                                          new CircularProgressIndicator(
                                                        valueColor:
                                                            new AlwaysStoppedAnimation<
                                                                    Color>(
                                                                redColor),
                                                      ),
                                                    ),
                                                    Center(
                                                      child: FadeInImage
                                                          .memoryNetwork(
                                                        placeholder:
                                                            kTransparentImage,
                                                        image: RestData
                                                                .IMAGE_URL +
                                                            galleryListDataEvent[
                                                                    itemIndex][
                                                                "ProfilePhoto"],
                                                      ),
                                                    ),
                                                    // Center(
                                                    //   child: new Image.asset(
                                                    //     'assets/products/${listDataAPI[index]["Name"]}.png',
                                                    //   ),
                                                    // ),
                                                  ],
                                                )),
                                          )),
                                    ));
                              },
                            ),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            height: 35,
                            // color: redColor,
                            child: Center(
                              child: new Text(
                                "Exhibitions",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0,
                                ),
                                maxLines: 2,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          SizedBox(height: 10.0),
                          SizedBox(
                            height: screenSize.width / 1.75,
                            child: PageView.builder(
                              controller:
                                  PageController(viewportFraction: 0.85),
                              itemCount: galleryListDataExhibition.length,
                              itemBuilder:
                                  (BuildContext context, int itemIndex) {
                                return Container(
                                    color: backgroundGrey,
                                    margin: new EdgeInsets.fromLTRB(5, 0, 5, 0),
                                    child: GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    GalleryDetailsPage(
                                                      selectedListData:
                                                          galleryListDataExhibition[
                                                                  itemIndex]
                                                              ["GalleryImages"],
                                                      titleName:
                                                          galleryListDataExhibition[
                                                                  itemIndex]
                                                              ["Name"],
                                                    )));
                                        print(galleryListDataExhibition[
                                            itemIndex]);
                                      },
                                      child: new GridTile(
                                          footer: new Center(
                                            child: Container(
                                              color: backgroundGrey,
                                              child: new Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 3.0,
                                                    left: 2.0,
                                                    right: 2.0),
                                                child: new AutoSizeText(
                                                  '${galleryListDataExhibition[itemIndex]["Name"]}',
                                                  style: new TextStyle(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.bold,
                                                    color: redColor,
                                                  ),
                                                  maxLines: 1,
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            ),
                                          ),

                                          // new Text("data[index]['name']"),
                                          child: new Center(
                                            child: new Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 0.0,
                                                    bottom: 30.0,
                                                    right: 0.0,
                                                    top: 0.0),
                                                child: Stack(
                                                  children: <Widget>[
                                                    Center(
                                                      child:
                                                          new CircularProgressIndicator(
                                                        valueColor:
                                                            new AlwaysStoppedAnimation<
                                                                    Color>(
                                                                redColor),
                                                      ),
                                                    ),
                                                    Center(
                                                      child: FadeInImage
                                                          .memoryNetwork(
                                                        placeholder:
                                                            kTransparentImage,
                                                        image: RestData
                                                                .IMAGE_URL +
                                                            galleryListDataExhibition[
                                                                    itemIndex][
                                                                "ProfilePhoto"],
                                                      ),
                                                    ),
                                                    // Center(
                                                    //   child: new Image.asset(
                                                    //     'assets/products/${listDataAPI[index]["Name"]}.png',
                                                    //   ),
                                                    // ),
                                                  ],
                                                )),
                                          )),
                                    ));
                              },
                            ),
                          ),
                        ])
                  : new Stack(
                      children: [
                        new Opacity(
                          opacity: 0.3,
                          child: const ModalBarrier(
                              dismissible: false, color: Colors.grey),
                        ),
                        new Center(
                          child: new CircularProgressIndicator(
                            valueColor:
                                new AlwaysStoppedAnimation<Color>(redColor),
                          ),
                        ),
                      ],
                    ),
            )));
  }

  Future _showSnackBar(String text) async {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  _getUserData() async {
    var db = new DatabaseHelper();
    User user1 = await db.getData();
    setState(() {
      user = user1;
      _getGallerynData();
    });
  }

  void _getGallerynData() async {
    RestData api = new RestData();
    setState(() => _isLoading = true);

    try {
      setState(() => _isLoading = true);
      var request = await api.getGalleryDetails();
      print(request);
      if (request["Response"] == true) {
        setState(() {
          _isLoading = false;
          List tempData = request["Result"];
          galleryListDataEvent.clear();
          galleryListDataExhibition.clear();

          for (int i = 0; i < tempData.length; i++) {
            var mytempData = tempData[i];
            var productData = mytempData["GalleryType"];

            if (productData == "Event") {
              galleryListDataEvent.add(mytempData);
            } else if (productData == "Exhibition") {
              galleryListDataExhibition.add(mytempData);
            }
          }
        });
      } else {
        _showSnackBar(request["Message"].toString());
        print("view");
      }
      setState(() => _isLoading = false);
    } catch (error) {
      _showSnackBar(error.toString());
    }
  }
}
