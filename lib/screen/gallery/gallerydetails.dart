import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';

import 'package:transparent_image/transparent_image.dart';

import '../../const.dart';

class GalleryDetailsPage extends StatefulWidget {
  GalleryDetailsPage({Key key, this.selectedListData, this.titleName})
      : super(key: key);
  final List selectedListData;
  final titleName;

  @override
  GalleryDetailsState createState() =>
      GalleryDetailsState(selectedListData, titleName);
}

class GalleryDetailsState extends State<GalleryDetailsPage> {
  GalleryDetailsState(this.selectedListData, this.titleName);
  List selectedListData;

  var titleName;
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          elevation: 10.0,
          backgroundColor: redColor,
          iconTheme: IconThemeData(
            color: Colors.white,
            //change your color here
          ),
          title: Text(
            titleName.toString(),
            style: TextStyle(
              color: whiteColor,
            ),
          ),
          // actions: <Widget>[
          //   new IconButton(
          //       icon: new Image.asset("assets/menu/menu-btn.png",
          //           height: 30.0, width: 30.0),
          //       onPressed: () {
          //         Scaffold.of(context).openEndDrawer();
          //       })
          // ],
        ),
        key: scaffoldKey,
        body: Container(
            child: PhotoViewGallery.builder(
          scrollPhysics: const BouncingScrollPhysics(),
          builder: (BuildContext context, int index) {
            return PhotoViewGalleryPageOptions(
              imageProvider: NetworkImage(
                  RestData.IMAGE_URL + selectedListData[index]["ImagePath"]),
              initialScale: PhotoViewComputedScale.contained * 0.8,
              // heroAttributes: HeroAttributes(tag: selectedListData[index].id),
            );
          },
          itemCount: selectedListData.length,

          loadingChild: Center(
            child: new CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(redColor),
            ),
          ),
          backgroundDecoration: BoxDecoration(
            color: Theme.of(context).canvasColor,
          ),

          // loadingChild: (context, event) => Center(
          //   child: Container(
          //     width: 20.0,
          //     height: 20.0,
          //     child: CircularProgressIndicator(
          //       value: event == null
          //           ? 0
          //           : event.cumulativeBytesLoaded / event.expectedTotalBytes,
          //     ),
          //   ),
          // ),
          // backgroundDecoration: backgroundGrey,
          // pageController: widget.pageController,
          // onPageChanged: () {},
        )));
  }

  void _showSnackBar(String text) {
    // setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
}
