import 'package:flutter/material.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/screen/drawer.dart';
import 'package:tectronics/screen/product/productdetails.dart';
import 'package:transparent_image/transparent_image.dart';

import '../const.dart';

class AboutUSPage extends StatefulWidget {
  AboutUSPage({Key key, this.user}) : super(key: key);
  final User user;

  @override
  AboutUSState createState() => AboutUSState(user);
}

class AboutUSState extends State<AboutUSPage> {
  User _user;
  AboutUSState(this._user);

  var db = new DatabaseHelper();
  List listOfProduct = [];

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    _getUserData();
  }

  _getUserData() async {
    var db = new DatabaseHelper();
    User user = await db.getData();
    print(user.name);
    setState(() {
      _user = user;
    });
  }

  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        backgroundColor: whiteColor,
        drawer: DrawerOnly(user: _user),
        appBar: AppBar(
          centerTitle: true,
          elevation: 10.0,
          backgroundColor: redColor,
          iconTheme: IconThemeData(
            color: Colors.white,
            //change your color here
          ),
          title: Text(
            "ABOUT US",
            style: TextStyle(
              color: whiteColor,
            ),
          ),
        ),
        key: scaffoldKey,
        body: !_isLoading
            ? ListView(
                shrinkWrap: false,
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                children: <Widget>[
                    // logo,
                    SizedBox(height: 20),

                    Container(
                        height: 80.0,
                        child: Center(
                          child: new Image.asset('assets/logo.png',
                              height: 50.0, width: 358.0),
                        )),

                    SizedBox(height: 20),

                    // Text(
                    //   "About US",
                    //   textAlign: TextAlign.center,
                    //   style:
                    //       TextStyle(fontSize: 25, fontWeight: FontWeight.w900),
                    // ),
                    // SizedBox(height: 10),
                    Text(
                      "Tectronics Engineers is one of the leading and accomplished companies in the Production and Quality Manufacturing of Gearless Elevator Machines in the market today. The Company has come up with the latest technical procedures and practices. The company has an assortment of solutions to the customers. It gives the customers best of all services along with Cost Effective, Economical and Affordable price range. Tectronics' employees and experts are always engaged in continual innovation and quality improvement in all processes and products. The Company boasts of their best service available with a team of experts, technicians, and professionals to cater to the customer's demands.",
                      textAlign: TextAlign.justify,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(height: 30),

                    Text(
                      "What Makes Tectronics",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 23, fontWeight: FontWeight.w900),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "We are manufacturer of Gearless Driving Machine which is incarnation of the Modern Elevator Technology providing Noise-free operations, Reduces storage space, Portable, Energy Efficient, Eco-Friendly and lowest in the Maintenance cost. We are highly striving to bring a notable change in the Elevator Technology by our R & D team.",
                      textAlign: TextAlign.justify,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),

                    SizedBox(height: 30),

                    Text(
                      "OUR VISION",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 23, fontWeight: FontWeight.w900),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "To Become India’s No.1 in Segment of Gearless Elevator Machine Manufacturing Company by 2025",
                      textAlign: TextAlign.justify,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),

                    SizedBox(height: 30),

                    Text(
                      "OUR MISSION",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 23, fontWeight: FontWeight.w900),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "As a Continues Process We’ll Work Hard to Provide Technologically Ultra Modern Product to Global Customer",
                      textAlign: TextAlign.justify,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),

                    SizedBox(height: 30),

                    Text(
                      "CORE VALUES",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 23, fontWeight: FontWeight.w900),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "Work with Honesty, Trust and Transparency Happiness in customer's life with satisfaction of our Best Quality product's performance Creating the future with continuous Change & Innovation Growing Together approach to develop our associates with trust, respect and value them in all our interaction and relation Growth with values and ethics",
                      textAlign: TextAlign.justify,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),

                    SizedBox(height: 30),
                  ])
            : new Stack(
                children: [
                  new Opacity(
                    opacity: 0.3,
                    child: const ModalBarrier(
                        dismissible: false, color: Colors.grey),
                  ),
                  new Center(
                    child: new CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(redColor),
                    ),
                  ),
                ],
              ));
  }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
}
