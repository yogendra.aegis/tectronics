import 'dart:ffi';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/screen/drawer.dart';
import 'package:tectronics/screen/product/productdetails.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../const.dart';

class ProductSearchPage extends StatefulWidget {
  ProductSearchPage({Key key, this.user}) : super(key: key);
  final User user;

  @override
  ProductSearchState createState() => ProductSearchState(user);
}

class ProductSearchState extends State<ProductSearchPage> {
  User _user;
  ProductSearchState(this._user);

  var db = new DatabaseHelper();

  List<dynamic> mainListofCategory = [];
  List listOfProduct = [];
  dynamic allData;

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;

  bool _isFilter = true;

  List<dynamic> _maxPassengerCapacitylDropDown = []; // Option 2
  dynamic _selectedMaxPassengerCapacity;

  List<dynamic> _maxSpeedDropDown = []; // Option 2
  dynamic _selectedMaxSpeed;

  // RangeValues rangeValueMaxPass = RangeValues(4, 36);
  // RangeValues rangeValueMaxSpeed = RangeValues(0, 2);

  @override
  void initState() {
    super.initState();

    _maxPassengerCapacitylDropDown = [
      // {
      //   "pass": 4,
      //   "speed": [0.32]
      // },
      // {
      //   "pass": 6,
      //   "speed": [0.5, 0.7, 1, 1.25, 1.5]
      // },
      // {
      //   "pass": 8,
      //   "speed": [0.5, 0.7, 1, 1.25, 1.5, 1.75, 2]
      // },
      // {
      //   "pass": 10,
      //   "speed": [0.5, 0.7, 1, 1.5, 2]
      // },
      // {
      //   "pass": 12,
      //   "speed": [0.5, 1]
      // },
      // {
      //   "pass": 13,
      //   "speed": [1, 1.5, 2]
      // },
      // {
      //   "pass": 14,
      //   "speed": [0.5, 1]
      // },
      // {
      //   "pass": 16,
      //   "speed": [0.5, 1, 1.5, 2]
      // },
      // {
      //   "pass": 20,
      //   "speed": [1, 1.5, 2]
      // },
      // {
      //   "pass": 24,
      //   "speed": [1]
      // },
      // {
      //   "pass": 30,
      //   "speed": [1]
      // },
      // {
      //   "pass": 36,
      //   "speed": [1]
      // },
    ];
    // _selectedMaxPassengerCapacity = _maxPassengerCapacitylDropDown[0];

    // _maxSpeedDropDown = _maxPassengerCapacitylDropDown[0]["speed"];
    // _selectedMaxSpeed = _maxSpeedDropDown[0];

    getAllCategory();
    _getUserData();
  }

  _getUserData() async {
    var db = new DatabaseHelper();
    User user = await db.getData();
    print(user.name);
    setState(() {
      _user = user;
    });
  }

  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        backgroundColor: whiteColor,
        drawer: DrawerOnly(user: _user),
        appBar: AppBar(
          // leading: IconButton(
          //   icon: Icon(Icons.arrow_back),
          //   onPressed: () {
          //     Navigator.pop(context);
          //   },
          // ),
          centerTitle: true,
          elevation: 10.0,
          backgroundColor: redColor,
          iconTheme: IconThemeData(
            color: Colors.white,
            //change your color here
          ),
          title: Text(
            "PRODUCT SELECTOR",
            style: TextStyle(
              color: whiteColor,
            ),
          ),

          actions: <Widget>[
            !_isFilter
                ? new IconButton(
                    icon: new Icon(Icons.filter_list, color: whiteColor),
                    onPressed: () {
                      setState(() {
                        _isFilter = (_isFilter == true) ? false : true;
                        if (_isFilter == false) {
                          updatedDataWhenFilterTap();
                        }
                        print("isfilter" + _isFilter.toString());
                      });
                    })
                : Container()
          ],
        ),
        key: scaffoldKey,
        body: !_isLoading
            ? !_isFilter
                ? new GridView.builder(
                    itemCount: listOfProduct == null ? 0 : listOfProduct.length,
                    padding: const EdgeInsets.all(4.0),
                    gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount:
                            (orientation == Orientation.portrait) ? 2 : 3),
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                          color: backgroundGrey,
                          // width: 160.0,
                          // color: Color(0xee0290).withOpacity(1.0),// colorData[index],
                          margin: new EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => ProductDetailsPage(
                                      selectedProduct: listOfProduct[index])));
                            },
                            child: new GridTile(
                              footer: new Center(
                                child: new Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 8.0, left: 2.0, right: 2.0),
                                  child: new AutoSizeText(
                                    listOfProduct[index]["ProductName"]
                                        .toString(),
                                    style: new TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: redColor,
                                        fontSize: 20.0),
                                    maxLines: 1,
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),

                              // new Text("data[index]['name']"),
                              child: new Center(
                                child: new Padding(
                                    padding: const EdgeInsets.only(
                                        left: 35.0,
                                        bottom: 45.0,
                                        right: 35.0,
                                        top: 25.0),
                                    child: Stack(
                                      children: <Widget>[
                                        Center(
                                          child: new CircularProgressIndicator(
                                            valueColor:
                                                new AlwaysStoppedAnimation<
                                                    Color>(redColor),
                                          ),
                                        ),
                                        Center(
                                          child: FadeInImage.memoryNetwork(
                                            placeholder: kTransparentImage,
                                            image: listOfProduct[index]
                                                ["ThumbImage"],
                                          ),
                                        ),
                                      ],
                                    )),
                              ),
                            ),
                          ));
                    },
                  )
                : ListView(
                    shrinkWrap: false,
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    children: <Widget>[
                        // logo,
                        SizedBox(height: 20),

                        new FormField(builder: (FormFieldState state) {
                          return InputDecorator(
                              decoration: InputDecoration(
                                hintText: 'Max. Passenger Capacity',
                                labelText: 'Max. Passenger Capacity',
                                filled: true,
                                fillColor: formElementColor,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(0),
                                  borderSide: new BorderSide(
                                    color: formElementColor,
                                    width: 2.0,
                                  ),
                                ),
                                contentPadding:
                                    EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                              ),
                              child: new DropdownButtonHideUnderline(
                                child: new DropdownButton(
                                  value: _selectedMaxPassengerCapacity,
                                  isDense: true,
                                  onChanged: (newValue) {
                                    setState(() {
                                      _maxSpeedDropDown = newValue["speed"];
                                      _selectedMaxSpeed = _maxSpeedDropDown[0];
                                      _selectedMaxPassengerCapacity = newValue;
                                    });
                                  },
                                  items: _maxPassengerCapacitylDropDown
                                      .map((value) {
                                    return DropdownMenuItem(
                                      child: new Text(value["pass"].toString()),
                                      value: value,
                                    );
                                  }).toList(),
                                ),
                              ));
                        }),
                        SizedBox(height: 10.0),

                        new FormField(builder: (FormFieldState state) {
                          return InputDecorator(
                              decoration: InputDecoration(
                                hintText: 'Max. Speed (m/s)',
                                labelText: 'Max. Speed (m/s)',
                                filled: true,
                                fillColor: formElementColor,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(0),
                                  borderSide: new BorderSide(
                                    color: formElementColor,
                                    width: 2.0,
                                  ),
                                ),
                                contentPadding:
                                    EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                              ),
                              child: new DropdownButtonHideUnderline(
                                child: new DropdownButton(
                                  value: _selectedMaxSpeed,
                                  isDense: true,
                                  onChanged: (newValue) {
                                    setState(() {
                                      // _dataProductModelDropDown = (newValue == "Sales") ? "1" : "2";
                                      _selectedMaxSpeed = newValue;
                                    });
                                  },
                                  items: _maxSpeedDropDown.map((value) {
                                    return DropdownMenuItem(
                                      child: new Text(value.toString()),
                                      value: value,
                                    );
                                  }).toList(),
                                ),
                              ));
                        }),
                        SizedBox(height: 10.0),

                        // ListView.builder(
                        //   scrollDirection: Axis.vertical,
                        //   shrinkWrap: true,
                        //   physics: new NeverScrollableScrollPhysics(),
                        //   itemCount: mainListofCategory == null
                        //       ? 0
                        //       : mainListofCategory.length,
                        //   itemBuilder: (BuildContext context, int index) {
                        //     return Card(
                        //         elevation: 3.0,
                        //         child: Container(
                        //             decoration: BoxDecoration(
                        //               color: formElementColor,
                        //             ),
                        //             child: new ListTile(
                        //                 title: new Row(
                        //               children: <Widget>[
                        //                 new Expanded(
                        //                     child: new Text(
                        //                         mainListofCategory[index]
                        //                             ["Name"])),
                        //                 new Checkbox(
                        //                     checkColor: whiteColor,
                        //                     activeColor: redColor,
                        //                     value: mainListofCategory[index]
                        //                         ["isSelected"],
                        //                     onChanged: (bool value) {
                        //                       setState(() {
                        //                         mainListofCategory[index]
                        //                             ["isSelected"] = value;
                        //                       });
                        //                     })
                        //               ],
                        //             ))));
                        //   }, //itemBuilder
                        // ),
                        // SizedBox(height: 20),

                        // Container(
                        //     decoration: BoxDecoration(
                        //       color: formElementColor,
                        //     ),
                        //     child: new ListTile(
                        //       title: new Text("Max. Passenger Capacity"),
                        //       subtitle: Row(
                        //         children: <Widget>[
                        //           SizedBox(
                        //             width: 25,
                        //             child: new Text(
                        //                 rangeValueMaxPass.start
                        //                     .toInt()
                        //                     .toString(),
                        //                 textAlign: TextAlign.center),
                        //           ),
                        //           Expanded(
                        //             child: RangeSlider(
                        //                 activeColor: redColor,
                        //                 values: rangeValueMaxPass,
                        //                 min: 4,
                        //                 max: 36,
                        //                 onChanged: (RangeValues values) {
                        //                   setState(() {
                        //                     if (values.end - values.start >=
                        //                         2) {
                        //                       rangeValueMaxPass = values;
                        //                     } else {
                        //                       if (rangeValueMaxPass.start ==
                        //                           values.start) {
                        //                         rangeValueMaxPass = RangeValues(
                        //                             values.start,
                        //                             values.start + 2);
                        //                       } else {
                        //                         rangeValueMaxPass = RangeValues(
                        //                             values.end - 2, values.end);
                        //                       }
                        //                     }
                        //                   });
                        //                 }),
                        //           ),
                        //           SizedBox(
                        //             width: 25,
                        //             child: new Text(
                        //                 rangeValueMaxPass.end
                        //                     .toInt()
                        //                     .toString(),
                        //                 textAlign: TextAlign.center),
                        //           ),
                        //         ],
                        //       ),
                        //       // RangeValues values = RangeValues(0.3, 0.7);
                        //     )),

                        // SizedBox(height: 20),

                        // Container(
                        //     decoration: BoxDecoration(
                        //       color: formElementColor,
                        //     ),
                        //     child: new ListTile(
                        //       title: new Text("Max. Speed (m/s)"),
                        //       subtitle: Row(
                        //         children: <Widget>[
                        //           SizedBox(
                        //             width: 25,
                        //             child: new Text(
                        //                 rangeValueMaxSpeed.start
                        //                     .toStringAsFixed(1),
                        //                 textAlign: TextAlign.center),
                        //           ),
                        //           Expanded(
                        //             child: RangeSlider(
                        //                 activeColor: redColor,
                        //                 values: rangeValueMaxSpeed,
                        //                 min: 0,
                        //                 max: 2.0,
                        //                 onChanged: (RangeValues values) {
                        //                   setState(() {
                        //                     if (values.end - values.start >=
                        //                         0.1) {
                        //                       rangeValueMaxSpeed = values;
                        //                     } else {
                        //                       if (rangeValueMaxSpeed.start ==
                        //                           values.start) {
                        //                         rangeValueMaxSpeed =
                        //                             RangeValues(values.start,
                        //                                 values.start + 0.1);
                        //                       } else {
                        //                         rangeValueMaxSpeed =
                        //                             RangeValues(
                        //                                 values.end - 0.1,
                        //                                 values.end);
                        //                       }
                        //                     }
                        //                   });
                        //                 }),
                        //           ),
                        //           SizedBox(
                        //             width: 25,
                        //             child: new Text(
                        //                 rangeValueMaxSpeed.end
                        //                     .toStringAsFixed(1),
                        //                 textAlign: TextAlign.center),
                        //           ),
                        //         ],
                        //       ),
                        //       // RangeValues values = RangeValues(0.3, 0.7);
                        //     )),
                        // SizedBox(height: 20),
                        Align(
                            alignment: Alignment.bottomRight,
                            child: RaisedButton(
                              onPressed: () {
                                setState(() {
                                  _isFilter =
                                      (_isFilter == true) ? false : true;
                                  if (_isFilter == false) {
                                    updatedDataWhenFilterTap();
                                  }
                                  print("isfilter" + _isFilter.toString());
                                });
                              },
                              // padding: EdgeInsets.all(10),
                              color: redColor,
                              child: Text(
                                "Ok",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22.0,
                                ),
                              ),
                            ))
                      ])
            : new Stack(
                children: [
                  new Opacity(
                    opacity: 0.3,
                    child: const ModalBarrier(
                        dismissible: false, color: Colors.grey),
                  ),
                  new Center(
                    child: new CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(redColor),
                    ),
                  ),
                ],
              ));
  }

  void updatedDataWhenFilterTap() {
    listOfProduct.clear();
    // dummyListOfCategory.clear();
    List<dynamic> thirdList = [];
    List<dynamic> secondList = List.from(thirdList);
    for (int x = 0; x < mainListofCategory.length; x++) {
      secondList.add(mainListofCategory[x]);
    }

    for (int i = 0; i < secondList.length; i++) {
      List productsData = secondList[i]["productDetail"];
      print(productsData.length);
      for (int j = 0; j < productsData.length; j++) {
        var productData = productsData[j];
        List specificationsData = productData["SpecificationData"];
        List newSpecificationsData = [];
        for (int k = 0; k < specificationsData.length; k++) {
          var specificationData = specificationsData[k];
          var maxpasscap =
              int.parse(specificationData["PassengerCapacityUpto"]);
          var maxspeed = double.parse(specificationData["SpeedUpto"]);

          if (maxpasscap == _selectedMaxPassengerCapacity["pass"] &&
              maxspeed == _selectedMaxSpeed) {
            newSpecificationsData.add(specificationData);
          }
        }
        if (newSpecificationsData.length > 0) {
          productData["Specification"] = newSpecificationsData;
          listOfProduct.add(productData);
        }
      }
    }
  }

  void getAllCategory() async {
    RestData api = new RestData();
    setState(() => _isLoading = true);

    try {
      setState(() => _isLoading = true);
      var request = await api.getAllProducts();
      print(request);
      if (request["Response"] == true) {
        setState(() {
          _isLoading = false;
          var createProductSearchData = [];
          List<dynamic> myTempList = request["Result"];
          for (var myTempCat in myTempList) {
            List myProducts = myTempCat["productDetail"];
            for (var myProduct in myProducts) {
              List specificationsData = myProduct["Specification"];
              for (var specificationsData in specificationsData) {
                var maxpasscap =
                    int.parse(specificationsData["PassengerCapacityUpto"]);
                if (!createProductSearchData.contains(maxpasscap)) {
                  createProductSearchData.add(maxpasscap);
                }
              }
              myProduct["SpecificationData"] = specificationsData;
            }
            mainListofCategory.add(myTempCat);
          }

          createProductSearchData.sort();

          print(createProductSearchData);
          _maxPassengerCapacitylDropDown.clear();

          for (var data in createProductSearchData) {
            Map<String, dynamic> map1 = {
              "pass": data,
              "speed": getAllAvailableData(myTempList, data)
            };
            _maxPassengerCapacitylDropDown.add(map1);
          }
          _selectedMaxPassengerCapacity = _maxPassengerCapacitylDropDown[0];
          _maxSpeedDropDown = _maxPassengerCapacitylDropDown[0]["speed"];
          _selectedMaxSpeed = _maxSpeedDropDown[0];
        });
      } else {
        _showSnackBar(request["Message"].toString());
        print("view");
      }
      setState(() => _isLoading = false);
    } catch (error) {
      _showSnackBar(error.toString());
    }
  }

  List getAllAvailableData(dynamic myTempList, int pass) {
    List templistdata = [];

    for (var myTempCat in myTempList) {
      List myProducts = myTempCat["productDetail"];
      for (var myProduct in myProducts) {
        List specificationsDatas = myProduct["Specification"];
        for (var specificationsData in specificationsDatas) {
          var maxpasscap =
              int.parse(specificationsData["PassengerCapacityUpto"]);
          var maxspeed = double.parse(specificationsData["SpeedUpto"]);

          if (pass == maxpasscap && !templistdata.contains(maxspeed)) {
            templistdata.add(maxspeed);
          }
        }
      }
    }

    templistdata.sort((a, b) => a.compareTo(b));

    return templistdata;
  }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
}

// class ProductModel {
//   int categoryId;
//   String name;
//   List<ProductModel> productDetail;
//   ProductModel(this.categoryId, this.name);

//   ProductModel.clone(ProductModel source)
//       : this.categoryId = source.categoryId,
//         this.name = source.name,
//         this.productDetail = source.productDetail
//             .map((item) => new ProductModel.clone(item))
//             .toList();
// }

// class ProductModel {
//   int categoryId;
//   String name;
//   List<dynamic> productDetail;

//   ProductModel(this.categoryId, this.name, this.productDetail);

//   ProductModel.fromJson(Map<String, dynamic> obj) {
//     this.categoryId = obj["CategoryId"];
//     this.name = obj["Name"].toString();
//     this.productDetail = obj["ProductDetail"];
//   }

//   // int get categoryId => categoryId;
//   // String get name => _name;
//   // dynamic get productDetail => productDetail;

//   // set categoryId(int value) => categoryId = value;
//   // set name(String value) => name = value;
//   // set productDetail(dynamic value) => productDetail = value;

//   Map<String, dynamic> toJson() {
//     var map = new Map<String, dynamic>();

//     map["CategoryId"] = categoryId;
//     map["Name"] = name;
//     map["ProductDetail"] = productDetail;

//     return map;
//   }
// }
