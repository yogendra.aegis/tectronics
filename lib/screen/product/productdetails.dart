import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';

import 'package:transparent_image/transparent_image.dart';

import '../../const.dart';
import '../../loading.dart';

class ProductDetailsPage extends StatefulWidget {
  ProductDetailsPage({Key key, this.selectedProduct}) : super(key: key);
  final dynamic selectedProduct;

  @override
  ProductDetailsState createState() => ProductDetailsState(selectedProduct);
}

class ProductDetailsState extends State<ProductDetailsPage> {
  ProductDetailsState(this.selectedProduct);
  var selectedProduct;

  User _user;

  var db = new DatabaseHelper();

  List listDataAPI = [];

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;

  List<dynamic> _dataProductModelDropDown = []; // Option 2
  dynamic _selectedDataProductModel;

  @override
  void initState() {
    super.initState();
    _dataProductModelDropDown = selectedProduct["Specification"];
    _selectedDataProductModel = _dataProductModelDropDown[0];
  }

  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          elevation: 10.0,
          backgroundColor: redColor,
          iconTheme: IconThemeData(
            color: Colors.white,
            //change your color here
          ),
          title: Text(
            selectedProduct["ProductName"].toString(),
            style: TextStyle(
              color: whiteColor,
            ),
          ),
          actions: <Widget>[
            new IconButton(
                icon: Icon(Icons.share),
                onPressed: () {
                  generatePDFFileofView();
                })
          ],
        ),
        key: scaffoldKey,
        body: new SafeArea(
            top: false,
            bottom: false,
            child: new Container(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              // height: 200.0,
              child: !_isLoading
                  ? ListView(
                      shrinkWrap: false,
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      children: <Widget>[
                          SizedBox(height: 20.0),
                          _buildTopImage(screenSize),
                          SizedBox(height: 20.0),
                          Container(
                            height: 35,
                            color: redColor,
                            child: Center(
                              child: new Text(
                                "Technical Specifications",
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0,
                                    color: whiteColor,
                                    backgroundColor: redColor),
                                maxLines: 2,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          SizedBox(height: 10.0),
                          new Container(
                            decoration:
                                new BoxDecoration(color: Colors.transparent),
                            child: Table(
                                border: TableBorder.all(
                                    width: 1.0, color: Colors.black),
                                children: [
                                  TableRow(children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 2.0, 0, 2.0),
                                      child: Text(" Voltage:",
                                          style: TextStyle(fontSize: 18.0),
                                          textAlign: TextAlign.left),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 2.0, 0, 2.0),
                                      child: Text(
                                          selectedProduct["Voltage"].toString(),
                                          style: TextStyle(fontSize: 18.0),
                                          textAlign: TextAlign.center),
                                    ),
                                  ]),
                                  TableRow(children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 2.0, 0, 2.0),
                                      child: Text(" Pole:",
                                          style: TextStyle(fontSize: 18.0),
                                          textAlign: TextAlign.left),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 2.0, 0, 2.0),
                                      child: Text(
                                          selectedProduct["Pole"].toString(),
                                          style: TextStyle(fontSize: 18.0),
                                          textAlign: TextAlign.center),
                                    ),
                                  ]),
                                  TableRow(children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 2.0, 0, 2.0),
                                      child: Text(" Duty Cycle:",
                                          style: TextStyle(fontSize: 18.0),
                                          textAlign: TextAlign.left),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 2.0, 0, 2.0),
                                      child: Text(
                                          selectedProduct["DutyCycle"]
                                              .toString(),
                                          style: TextStyle(fontSize: 18.0),
                                          textAlign: TextAlign.center),
                                    ),
                                  ]),
                                  TableRow(children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 2.0, 0, 2.0),
                                      child: Text(" Brake Voltage:",
                                          style: TextStyle(fontSize: 18.0),
                                          textAlign: TextAlign.left),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 2.0, 0, 2.0),
                                      child: Text(
                                          selectedProduct["BrakeVoltage"]
                                              .toString(),
                                          style: TextStyle(fontSize: 18.0),
                                          textAlign: TextAlign.center),
                                    ),
                                  ]),
                                  TableRow(children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 2.0, 0, 2.0),
                                      child: Text(" Max. Static Load:",
                                          style: TextStyle(fontSize: 18.0),
                                          textAlign: TextAlign.left),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0, 2.0, 0, 2.0),
                                      child: Text(
                                          selectedProduct["MaxStaticLoad"]
                                              .toString(),
                                          style: TextStyle(fontSize: 18.0),
                                          textAlign: TextAlign.center),
                                    ),
                                  ]),
                                ]),
                          ),
                          SizedBox(height: 10.0),
                          _buildCenterImage(screenSize),
                          SizedBox(height: 20.0),
                          new FormField(builder: (FormFieldState state) {
                            return InputDecorator(
                                decoration: InputDecoration(
                                  hintText: 'Select Model No.',
                                  labelText: 'Select Model No.',
                                  filled: true,
                                  fillColor: formElementColor,
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(0),
                                    borderSide: new BorderSide(
                                      color: formElementColor,
                                      width: 2.0,
                                    ),
                                  ),
                                  contentPadding: EdgeInsets.fromLTRB(
                                      10.0, 10.0, 10.0, 10.0),
                                ),
                                child: new DropdownButtonHideUnderline(
                                  child: new DropdownButton(
                                    value: _selectedDataProductModel,
                                    isDense: true,
                                    onChanged: (newValue) {
                                      setState(() {
                                        // _dataProductModelDropDown = (newValue == "Sales") ? "1" : "2";
                                        _selectedDataProductModel = newValue;
                                      });
                                    },
                                    items:
                                        _dataProductModelDropDown.map((value) {
                                      return DropdownMenuItem(
                                        child: new Text(value["ModelNo"]),
                                        value: value,
                                      );
                                    }).toList(),
                                  ),
                                ));
                          }),
                          SizedBox(height: 10.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Max. Duty Load(Kg.)",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["DutyLoad"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Max. Passenger Capacity",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel[
                                              "PassengerCapacityUpto"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Max. Speed (m/s)",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["SpeedUpto"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Sheave Dia (mm)",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["SheaveDia"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Ratio of Roping",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["Roping"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "No. of Ropes",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["NoOfRopes"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Rope Dia. (mm)",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["RopeDia"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Current (A)",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["Current"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Power (kw)",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["Power"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "RPM",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["Rpm"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Torque (NM)",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["Torque"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Frequency (Hz)",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["Frequency"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "IP Protection",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["IpProtection"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Start / Hr.",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["Start"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      "Machine Weight (kg) Appx",
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Expanded(
                                child: Container(
                                  height: 45,
                                  color: backgroundGrey,
                                  child: Center(
                                    child: new Text(
                                      _selectedDataProductModel["MachineWeight"]
                                          .toString(),
                                      style: new TextStyle(
                                          fontSize: 16.0, color: textColor),
                                      maxLines: 2,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 20.0),
                        ])
                  : new Stack(
                      children: [
                        new Opacity(
                          opacity: 0.2,
                          child: const ModalBarrier(
                              dismissible: false, color: Colors.grey),
                        ),
                        new Center(
                          child: new CircularProgressIndicator(
                            valueColor:
                                new AlwaysStoppedAnimation<Color>(redColor),
                          ),
                        ),
                      ],
                    ),
            )));
  }

  Widget _buildTopImage(Size screenSize) {
    return Container(
        height: screenSize.height / 4,
        child: GestureDetector(
          child: Stack(
            children: <Widget>[
              Center(
                child: new CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(redColor),
                ),
              ),
              Center(
                child: FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: selectedProduct["Image"],
                ),
              ),
            ],
          ),
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (_) {
              return DetailScreen(
                urlString: selectedProduct["Image"],
                titleString: selectedProduct["ProductName"].toString(),
              );
            }));
          },
        ));
  }

  Widget _buildCenterImage(Size screenSize) {
    return Container(
      height: screenSize.height / 4,
      child: GestureDetector(
        child: Stack(
          children: <Widget>[
            Center(
              child: new CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(redColor),
              ),
            ),
            Center(
              child: FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                image: selectedProduct["TechnicalImage"],
              ),
            ),
          ],
        ),
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (_) {
            return DetailScreen(
              urlString: selectedProduct["TechnicalImage"],
              titleString: selectedProduct["ProductName"].toString(),
            );
          }));
        },
      ),
    );
  }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  void generatePDFFileofView() async {
    RestData api = new RestData();
    setState(() => _isLoading = false);

    try {
      // setState(() => _isLoading = true);
      Dialogs.showLoadingDialog(
          context, globalScaffoldKey, "Generating PDF...");
      var request = await api.generatePDFFile(
          _selectedDataProductModel["ProductId"].toString(),
          _selectedDataProductModel["ModelNo"].toString());
      print(request);
      if (request["Response"] == true) {
        await _shareImageFromUrl(request["Result"]);
        setState(() {
          _isLoading = false;
        });
      } else {
        _showSnackBar(request["Message"].toString());
        print("view");
      }
      Navigator.of(globalScaffoldKey.currentContext, rootNavigator: true).pop();
      setState(() => _isLoading = false);
    } catch (error) {
      Navigator.of(globalScaffoldKey.currentContext, rootNavigator: true).pop();
      _showSnackBar(error.toString());
    }
  }

  Future<void> _shareImageFromUrl(dynamic request) async {
    try {
      var _bytesImage = Base64Decoder().convert(request["FileBytes"]);
      Uint8List bytes2 = new Uint8List.fromList(_bytesImage);
      await Share.file(
          _selectedDataProductModel["ProductName"].toString(),
          _selectedDataProductModel["ProductName"].toString() + ".pdf",
          bytes2,
          'application/pdf');
    } catch (e) {
      print('error: $e');
    }
  }
}

class DetailScreen extends StatelessWidget {
  final urlString;
  final titleString;

  const DetailScreen({Key key, this.urlString, this.titleString})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        elevation: 10.0,
        backgroundColor: redColor,
        iconTheme: IconThemeData(
          color: Colors.white,
          //change your color here
        ),
        title: Text(
          titleString,
          style: TextStyle(
            color: whiteColor,
          ),
        ),
        // actions: <Widget>[
        //   new IconButton(
        //       icon: new Image.asset("assets/menu/menu-btn.png",
        //           height: 30.0, width: 30.0),
        //       onPressed: () {
        //         Scaffold.of(context).openEndDrawer();
        //       })
        // ],
      ),
      body: GestureDetector(
        onTapDown: _tapDown,
        child: Center(
            child: PhotoView(
          imageProvider: NetworkImage(urlString),
          minScale: PhotoViewComputedScale.contained * 0.8,
          maxScale: 4.0,
        )),
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  void _tapDown(TapDownDetails details) {
    print("tap down");
  }
}
