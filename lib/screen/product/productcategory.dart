import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/screen/drawer.dart';
import 'package:tectronics/screen/product/productdetails.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../const.dart';

class ProductCategoryPage extends StatefulWidget {
  ProductCategoryPage({Key key, this.categoryDetails}) : super(key: key);
  final dynamic categoryDetails;

  @override
  ProductCategoryState createState() => ProductCategoryState(categoryDetails);
}

class ProductCategoryState extends State<ProductCategoryPage> {
  ProductCategoryState(this.categoryDetails);
  var categoryDetails;

  var db = new DatabaseHelper();
  User _user;

  List listProductByCategory = [];

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  @override
  void initState() {
    super.initState();
    setState(() {
      _isLoading = false;
      listProductByCategory = categoryDetails["productDetail"];
    });

    // getAllProductByCategory();
  }

  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        backgroundColor: whiteColor,
        drawer: DrawerOnly(user: _user),
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          elevation: 10.0,
          backgroundColor: redColor,
          iconTheme: IconThemeData(
            color: Colors.white,
            //change your color here
          ),
          title: Text(
            categoryDetails["Name"].toString(),
            style: TextStyle(
              color: whiteColor,
            ),
          ),
          // actions: <Widget>[
          //   new IconButton(
          //       icon: new Image.asset("assets/menu/menu-btn.png",
          //           height: 30.0, width: 30.0),
          //       onPressed: () {
          //         Scaffold.of(context).openEndDrawer();
          //       })
          // ],
        ),
        body: !_isLoading
            ? new GridView.builder(
                itemCount: listProductByCategory.length,
                padding: const EdgeInsets.all(4.0),
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount:
                        (orientation == Orientation.portrait) ? 2 : 3),
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    color: backgroundGrey,
                    margin: new EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ProductDetailsPage(
                                selectedProduct:
                                    listProductByCategory[index])));
                      },
                      child: new GridTile(
                          footer: new Center(
                            child: new Padding(
                              padding: const EdgeInsets.only(
                                  bottom: 8.0, left: 2.0, right: 2.0),
                              child: new AutoSizeText(
                                listProductByCategory[index]["ProductName"]
                                    .toString(),
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: redColor,
                                  fontSize: 20,
                                ),
                                maxLines: 1,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),

                          // new Text("data[index]['name']"),
                          child: new Center(
                            child: new Padding(
                                padding: const EdgeInsets.only(
                                    left: 35.0,
                                    bottom: 45.0,
                                    right: 35.0,
                                    top: 25.0),
                                child: Stack(
                                  children: <Widget>[
                                    Center(
                                      child: new CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                redColor),
                                      ),
                                    ),
                                    Center(
                                      child: FadeInImage.memoryNetwork(
                                        placeholder: kTransparentImage,
                                        image: listProductByCategory[index]
                                            ["ThumbImage"],
                                      ),
                                    ),
                                  ],
                                )),
                          )),
                    ),
                  );
                },
              )
            : new Stack(
                children: [
                  new Opacity(
                    opacity: 0.3,
                    child: const ModalBarrier(
                        dismissible: false, color: Colors.grey),
                  ),
                  new Center(
                    child: new CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(redColor),
                    ),
                  ),
                ],
              ));

    // );
  }

  // void getAllProductByCategory() async {
  //   RestData api = new RestData();
  //   setState(() => _isLoading = true);

  //   try {
  //     setState(() => _isLoading = true);
  //     var request = await api
  //         .getAllProductsByCategory(categoryDetails["CategoryId"].toString());
  //     if (request["Response"] == true) {
  //       setState(() {
  //         _isLoading = false;
  //         listProductByCategory = request["Result"];
  //       });
  //     } else {
  //       _showSnackBar(request["Message"].toString());
  //       print("view");
  //     }
  //     setState(() => _isLoading = false);
  //   } catch (error) {
  //     _showSnackBar(error.toString());
  //   }
  // }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
}
