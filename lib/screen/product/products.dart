import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/screen/drawer.dart';
import 'package:tectronics/screen/product/productdetails.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../const.dart';

class ProductsPage extends StatefulWidget {
  ProductsPage({Key key, this.user}) : super(key: key);
  final User user;

  @override
  ProductsState createState() => ProductsState(user);
}

class ProductsState extends State<ProductsPage> {
  User _user;
  ProductsState(this._user);

  var db = new DatabaseHelper();
  List listOfProduct = [];

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    getAllCategory();
    _getUserData();
  }

  _getUserData() async {
    var db = new DatabaseHelper();
    User user = await db.getData();
    print(user.name);
    setState(() {
      _user = user;
    });
  }

  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        backgroundColor: whiteColor,
        drawer: DrawerOnly(user: _user),
        appBar: AppBar(
          centerTitle: true,
          elevation: 10.0,
          backgroundColor: redColor,
          iconTheme: IconThemeData(
            color: Colors.white,
            //change your color here
          ),
          title: Text(
            "OUR PRODUCTS",
            style: TextStyle(
              color: whiteColor,
            ),
          ),
        ),
        key: scaffoldKey,
        body: !_isLoading
            ? new GridView.builder(
                itemCount: listOfProduct == null ? 0 : listOfProduct.length,
                padding: const EdgeInsets.all(4.0),
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount:
                        (orientation == Orientation.portrait) ? 2 : 3),
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                      color: backgroundGrey,
                      // width: 160.0,
                      // color: Color(0xee0290).withOpacity(1.0),// colorData[index],
                      margin: new EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ProductDetailsPage(
                                  selectedProduct: listOfProduct[index])));
                        },
                        child: new GridTile(
                          footer: new Center(
                            child: new Padding(
                              padding: const EdgeInsets.only(
                                  bottom: 8.0, left: 2.0, right: 2.0),
                              child: new AutoSizeText(
                                listOfProduct[index]["ProductName"].toString(),
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: redColor,
                                    fontSize: 20.0),
                                maxLines: 1,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),

                          // new Text("data[index]['name']"),
                          child: new Center(
                            child: new Padding(
                                padding: const EdgeInsets.only(
                                    left: 35.0,
                                    bottom: 45.0,
                                    right: 35.0,
                                    top: 25.0),
                                child: Stack(
                                  children: <Widget>[
                                    Center(
                                      child: new CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                redColor),
                                      ),
                                    ),
                                    Center(
                                      child: FadeInImage.memoryNetwork(
                                        placeholder: kTransparentImage,
                                        image: listOfProduct[index]
                                            ["ThumbImage"],
                                      ),
                                    ),
                                  ],
                                )),
                          ),
                        ),
                      ));
                },
              )
            : new Stack(
                children: [
                  new Opacity(
                    opacity: 0.3,
                    child: const ModalBarrier(
                        dismissible: false, color: Colors.grey),
                  ),
                  new Center(
                    child: new CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(redColor),
                    ),
                  ),
                ],
              ));
  }

  void getAllCategory() async {
    RestData api = new RestData();
    setState(() => _isLoading = true);

    try {
      setState(() => _isLoading = true);
      var request = await api.getAllProducts();
      print(request);
      if (request["Response"] == true) {
        setState(() {
          _isLoading = false;
          List tempData = request["Result"];
          // mainListofCategory.clear();
          for (int i = 0; i < tempData.length; i++) {
            var mytempData = tempData[i];
            mytempData["isSelected"] = true;
            print(mytempData);
            // mainListofCategory.add(mytempData);
            var productData = mytempData["productDetail"];
            for (var product in productData) {
              print(product);
              listOfProduct.add(product);
            }
          }
        });
      } else {
        _showSnackBar(request["Message"].toString());
        print("view");
      }
      setState(() => _isLoading = false);
    } catch (error) {
      _showSnackBar(error.toString());
    }
  }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
}
