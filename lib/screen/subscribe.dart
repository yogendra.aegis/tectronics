import 'package:flutter/material.dart';
import './drawer.dart';

class SubscribePage extends StatefulWidget {
  @override
  _SubscribePageState createState() => _SubscribePageState();
}

class _SubscribePageState extends State<SubscribePage> {
  final _formKey = GlobalKey<FormState>();

  int _genderRadio = 0;
  String _name;
  String _email;
  String _mobile;
  String _city;
  String _gender;
  bool _autoValidate = false;

  void onChangedGender(int value) {
    setState(() {
      _genderRadio = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Color(0xffffffff),
      appBar: AppBar(
        elevation: 1.0,
        backgroundColor: Color(0xffed1c24),
        title: Text("Subscribe"),
      ),
      drawer: DrawerOnly(),
      body: new SingleChildScrollView(
        child: new Container(
          margin: new EdgeInsets.all(15.0),
          child: Form(
            key: _formKey,
            autovalidate: _autoValidate,
            child: formUI(),
          ),
        ),
      ),
    );
  }

  Widget formUI() {
    return new Column(
      children: <Widget>[
        // new Padding(
        //   padding: const EdgeInsets.all(8.0),
        //   child:
        TextFormField(
          decoration: const InputDecoration(
            labelText: 'Name',
          ),
          validator: validateName,
          onSaved: (String val) {
            _name = val;
          },
          keyboardType: TextInputType.text,
        ),
        // ),
        // new Padding(
        //   padding: const EdgeInsets.all(8.0),
        //   child:
        TextFormField(
          decoration: const InputDecoration(
            labelText: 'Email',
          ),
          validator: validateEmail,
          keyboardType: TextInputType.emailAddress,
          onSaved: (String val) {
            _email = val;
          },
        ),
        // ),
        // new Padding(
        //   padding: const EdgeInsets.all(8.0),
        //   child:
        TextFormField(
          decoration: const InputDecoration(
            labelText: 'Mobile',
          ),
          validator: validateMobile,
          keyboardType: TextInputType.phone,
          onSaved: (String val) {
            _mobile = val;
          },
        ),
        // ),
        // new Padding(
        //   padding: const EdgeInsets.all(8.0),
        //   child:
        new TextFormField(
          decoration: const InputDecoration(
            labelText: 'City',
          ),
          validator: validateCity,
          onSaved: (String val) {
            _city = val;
          },
          keyboardType: TextInputType.text,
        ),
        // ),
        // new Padding(
        //   padding: const EdgeInsets.all(8.0),
        //   child:
        new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              'Gender: ',
            ),
            new Radio(
              value: 0,
              groupValue: _genderRadio,
              onChanged: onChangedGender,
              activeColor: Color(0xffed1c24),
            ),
            new Text(
              'Male',
            ),
            new Radio(
              value: 1,
              groupValue: _genderRadio,
              onChanged: onChangedGender,
              activeColor: Color(0xffed1c24),
            ),
            new Text(
              'Female',
            ),
          ],
        ),
        // ),
        new SizedBox(
          height: 10.0,
        ),
        // new Padding(
        //   padding: const EdgeInsets.all(10.0),
        // child:
        new RaisedButton(
          onPressed: _validateInputs,
          child: new Text(
            "SUBSCRIBE",
            style: TextStyle(color: Colors.white),
          ),
          color: Color(0xffed1c24),
        ),
        // ),
      ],
    );
  }

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
      // If the form is valid, we want to show a Snackbar
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text('Processing Data')));
    } else {
      _autoValidate = true;
    }
  }
}

String validateName(String value) {
  if (value.length < 3)
    return 'Name must be more than 2 charater';
  else
    return null;
}

String validateCity(String value) {
  if (value.length < 3)
    return 'City must be more than 2 charater';
  else
    return null;
}

String validateMobile(String value) {
// Indian Mobile number are of 10 digit only
  if (value.length != 10)
    return 'Mobile Number must be of 10 digit';
  else
    return null;
}

String validateEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);
  if (!regex.hasMatch(value))
    return 'Enter Valid Email';
  else
    return null;
}
