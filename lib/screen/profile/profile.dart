import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/const.dart';
import 'package:tectronics/screen/profile/update_profile.dart';

import '../../const.dart';
import '../drawer.dart';
import 'change_password.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key, this.user}) : super(key: key);
  final User user;

  @override
  ProfileState createState() => ProfileState(user);
}

class ProfileState extends State<ProfilePage> {
  User user;
  ProfileState(this.user);

  bool _isLoading = false;

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  DateTime selectedDate = DateTime.now();

  @override
  void initState() {
    super.initState();
    _isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      drawer: DrawerOnly(user: user),
      appBar: AppBar(
        centerTitle: true,
        elevation: 10.0,
        backgroundColor: redColor,
        iconTheme: IconThemeData(
          color: Colors.white,
          //change your color here
        ),
        title: Text(
          "Profile Info",
          style: TextStyle(
            color: whiteColor,
          ),
        ),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.edit),
              onPressed: () {
                _goToProfileEditPage(user);
              }),
          new IconButton(
              icon: new Icon(Icons.lock_open),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ChangePasswordPage(
                          user: user,
                        )));
              }),
        ],
      ),
      key: scaffoldKey,
      body: new SafeArea(
        top: false,
        bottom: false,
        child: Center(
          child: !_isLoading
              ? ListView(
                  shrinkWrap: false,
                  // padding: EdgeInsets.only(left: 30.0, right: 30.0),
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    new ListTile(
                        dense: true,
                        title: new Text(
                          (user?.name == null) ? "No Name" : user.name,
                          style: TextStyle(
                              color: redColor,
                              fontWeight: FontWeight.normal,
                              fontSize: 20.0),
                        ),
                        leading:
                            Icon(Icons.person, color: redColor, size: 25.0),
                        onTap: () {
                          // Navigator.of(context).pop();
                        }),
                    new Divider(
                      color: redColor,
                    ),
                    new ListTile(
                        dense: true,
                        title: new Text(
                          (user?.mobileNumber == null)
                              ? "No Mobile Number"
                              : user.mobileNumber,
                          style: TextStyle(
                              color: redColor,
                              fontWeight: FontWeight.normal,
                              fontSize: 20.0),
                        ),
                        leading: Icon(Icons.phone, color: redColor, size: 25.0),
                        onTap: () {
                          // Navigator.of(context).pop();
                        }),
                    new Divider(
                      color: redColor,
                    ),
                    new ListTile(
                        dense: true,
                        title: new Text(
                          (user?.email == null) ? "No Email" : user.email,
                          style: TextStyle(
                              color: redColor,
                              fontWeight: FontWeight.normal,
                              fontSize: 20.0),
                        ),
                        leading: Icon(Icons.email, color: redColor, size: 25.0),
                        onTap: () {
                          // Navigator.of(context).pop();
                        }),
                    new Divider(
                      color: redColor,
                    ),
                    new ListTile(
                        dense: true,
                        title: new Text(
                          (user?.companyName == null)
                              ? "No Company Name"
                              : user.companyName,
                          style: TextStyle(
                              color: redColor,
                              fontWeight: FontWeight.normal,
                              fontSize: 20.0),
                        ),
                        leading:
                            Icon(Icons.business, color: redColor, size: 25.0),
                        onTap: () {
                          // Navigator.of(context).pop();
                        }),
                    new Divider(
                      color: redColor,
                    ),
                    new ListTile(
                        dense: true,
                        title: new Text(
                          (user?.location == null)
                              ? "No Location"
                              : user.location,
                          style: TextStyle(
                              color: redColor,
                              fontWeight: FontWeight.normal,
                              fontSize: 20.0),
                        ),
                        leading: Icon(Icons.place, color: redColor, size: 25.0),
                        onTap: () {
                          // Navigator.of(context).pop();
                        }),
                    new Divider(
                      color: redColor,
                    ),
                    (user.gstNo == "" || user.gstNo == null)
                        ? Container()
                        : new ListTile(
                            dense: true,
                            title: new Text(
                              user.gstNo,
                              style: TextStyle(
                                  color: redColor,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 20.0),
                            ),
                            leading: Icon(Icons.keyboard,
                                color: redColor, size: 25.0),
                            onTap: () {
                              // Navigator.of(context).pop();
                            }),
                    (user.gstNo == "" || user.gstNo == null)
                        ? Container()
                        : new Divider(
                            color: redColor,
                          ),
                    (user.birthDate == "" || user.birthDate == null)
                        ? Container()
                        : new ListTile(
                            dense: true,
                            title: new Text(
                              user.birthDate,
                              style: TextStyle(
                                  color: redColor,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 20.0),
                            ),
                            leading: Icon(Icons.date_range,
                                color: redColor, size: 25.0),
                            onTap: () {
                              // Navigator.of(context).pop();
                            }),
                    (user.birthDate == "" || user.birthDate == null)
                        ? Container()
                        : new Divider(
                            color: redColor,
                          ),
                    (user.anniversaryDate == "" || user.anniversaryDate == null)
                        ? Container()
                        : new ListTile(
                            dense: true,
                            title: new Text(
                              user.anniversaryDate,
                              style: TextStyle(
                                  color: redColor,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 20.0),
                            ),
                            leading: Icon(Icons.date_range,
                                color: redColor, size: 25.0),
                            onTap: () {
                              // Navigator.of(context).pop();
                            }),
                    (user.anniversaryDate == "" || user.anniversaryDate == null)
                        ? Container()
                        : new Divider(
                            color: redColor,
                          ),
                  ],
                )
              : new Stack(
                  children: [
                    new Opacity(
                      opacity: 0.3,
                      child: const ModalBarrier(
                          dismissible: false, color: Colors.grey),
                    ),
                    new Center(
                      child: new CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(redColor),
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }

  Future _goToProfileEditPage(User userData) async {
    User userEditedData = await Navigator.of(context).push(
      MaterialPageRoute(
          builder: (context) => UpdateProfilePage(
                user: userData,
              )),
    );

    print("Before" + userEditedData.toString());

    if (userEditedData != null && userEditedData != user) {
      print("After" + userEditedData.toString());
      setState(() {
        user = userEditedData;
      });
    }
  }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
}
