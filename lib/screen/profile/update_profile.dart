import 'dart:ui';
import 'package:intl/intl.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/const.dart';
import 'package:tectronics/screen/login/otpverify.dart';

class UpdateProfilePage extends StatefulWidget {
  UpdateProfilePage({Key key, this.user}) : super(key: key);
  final User user;
  @override
  UpdateProfileState createState() => new UpdateProfileState(user);
}

class UpdateProfileState extends State<UpdateProfilePage> {
  User user;
  UpdateProfileState(this.user);

  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _textFieldControllerBirthdate = TextEditingController();
  TextEditingController _textFieldControllerAnniversaryBirthdate =
      TextEditingController();

  String _name,
      _companyName,
      _gstNO,
      _location,
      _birthDate,
      _annivarsaryDate,
      _email,
      _mobileNumber;
  bool _autoValidate = false;
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  DateTime selectedDate = DateTime.now();

  @override
  void initState() {
    super.initState();
    _isLoading = false;
    _name = user?.name;
    _companyName = user?.companyName;
    _gstNO = user?.gstNo;
    _location = user?.location;
    _birthDate = user?.birthDate;
    _annivarsaryDate = user?.anniversaryDate;
    _email = user?.email;
    _mobileNumber = user?.mobileNumber;

    _textFieldControllerBirthdate = new TextEditingController(text: _birthDate);
    _textFieldControllerAnniversaryBirthdate =
        new TextEditingController(text: _annivarsaryDate);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, user);
          },
        ),
        centerTitle: true,
        elevation: 10.0,
        backgroundColor: redColor,
        iconTheme: IconThemeData(
          color: Colors.white,
          //change your color here
        ),
        title: Text(
          "UPDATE PROFILE",
          style: TextStyle(
            color: whiteColor,
          ),
        ),
      ),
      key: scaffoldKey,
      body: new SafeArea(
        top: false,
        bottom: false,
        child: new Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Center(
            child: !_isLoading
                ? ListView(
                    shrinkWrap: false,
                    padding: EdgeInsets.only(left: 30.0, right: 30.0),
                    children: <Widget>[
                      SizedBox(height: 20.0),

                      TextFormField(
                        keyboardType: TextInputType.text,
                        initialValue: _name,
                        onChanged: (text) {
                          _name = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'Full Name',
                          labelText: 'Full Name',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.person,
                            color: redColor,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: formElementColor,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          if (value.length < 3) {
                            return ('Please enter valid Name.');
                          }
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(40),
                        ],
                        onSaved: (String val) {
                          _name = val;
                        },
                      ),

                      SizedBox(height: 10.0),
                      TextFormField(
                        enabled: false, 
                        keyboardType: TextInputType.number,
                        initialValue: _mobileNumber,
                        onChanged: (text) {
                          _mobileNumber = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'Mobile Number',
                          labelText: 'Mobile Number',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.dialpad,
                            color: redColor,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: formElementColor,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
                          RegExp regExp = new RegExp(patttern);
                          if (value.length == 0) {
                            return 'Please enter mobile number';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Please enter valid mobile number';
                          }
                          return null;
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(40),
                        ],
                        onSaved: (String val) {
                          _mobileNumber = val;
                        },
                      ),

                      SizedBox(height: 10.0),

                      TextFormField(
                        enabled: false, 
                        keyboardType: TextInputType.emailAddress,
                        initialValue: _email,
                        onChanged: (text) {
                          _email = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'Email',
                          labelText: 'Email',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.email,
                            color: redColor,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: formElementColor,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          String patttern =
                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
                          RegExp regExp = new RegExp(patttern);
                          if (value.length == 0) {
                            return 'Please enter Email Address';
                          } else if (!regExp.hasMatch(value)) {
                            return 'Please enter valid Email Address';
                          }
                          return null;
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(40),
                        ],
                        onSaved: (String val) {
                          _email = val;
                        },
                      ),

                      SizedBox(height: 10.0),

                      TextFormField(
                        keyboardType: TextInputType.text,
                        initialValue: _companyName,
                        onChanged: (text) {
                          _companyName = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'Company Name',
                          labelText: 'Company Name',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.business,
                            color: redColor,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: formElementColor,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          if (value.length < 3) {
                            return ('Please enter valid Company Name.');
                          }
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(40),
                        ],
                        onSaved: (String val) {
                          _companyName = val;
                        },
                      ),
                      SizedBox(height: 10.0),

                      TextFormField(
                        keyboardType: TextInputType.text,
                        // maxLines: 3,
                        initialValue: _location,
                        onChanged: (text) {
                          _location = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'Location',
                          labelText: 'Location',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.place,
                            color: redColor,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: formElementColor,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          if (value.length < 3) {
                            return ('Please enter valid location details.');
                          }
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(50),
                        ],
                        onSaved: (String val) {
                          _location = val;
                        },
                      ),

                      SizedBox(height: 10.0),

                      TextFormField(
                        keyboardType: TextInputType.text,
                        initialValue: _gstNO,
                        onChanged: (text) {
                          _gstNO = text;
                        },
                        decoration: InputDecoration(
                          hintText: 'GST No.',
                          labelText: 'GST No.',
                          filled: true,
                          fillColor: formElementColor,
                          prefixIcon: Icon(
                            Icons.keyboard,
                            color: redColor,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0),
                            borderSide: new BorderSide(
                              color: formElementColor,
                              width: 2.0,
                            ),
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        ),
                        validator: (value) {
                          String patttern =
                              r"^\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}";
                          RegExp regExp = new RegExp(patttern);
                          if (value.length == 0) {
                            return null;
                          } else if (!regExp.hasMatch(value)) {
                            return 'Please enter valid GST NO.';
                          }
                          return null;
                        },
                        inputFormatters: [
                          new LengthLimitingTextInputFormatter(15),
                        ],
                        onSaved: (String val) {
                          _gstNO = val;
                        },
                      ),
                      SizedBox(height: 10.0),

                      InkWell(
                        onTap: () {
                          _selectBirthDate();
                        },
                        child: IgnorePointer(
                          child: new TextFormField(
                            keyboardType: TextInputType.text,
                            controller: _textFieldControllerBirthdate,
                            decoration: InputDecoration(
                              hintText: 'Birthdate',
                               labelText: 'Birthdate',
                              filled: true,
                              fillColor: formElementColor,
                              prefixIcon: Icon(
                                Icons.date_range,
                                color: redColor,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0),
                                borderSide: new BorderSide(
                                  color: formElementColor,
                                  width: 2.0,
                                ),
                              ),
                              contentPadding:
                                  EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                            ),
                            onSaved: (String val) {
                              _birthDate = val;
                            },
                            validator: (value) {
                              if (value.length == 0) {
                                return 'Please select your Birthdate.';
                              }
                              return null;
                            },
                          ),
                        ),
                      ),
                      SizedBox(height: 10.0),
                      InkWell(
                        onTap: () {
                          _selectAnniversaryDate(); // Call Function that has showDatePicker()
                        },
                        child: IgnorePointer(
                          child: new TextFormField(
                            keyboardType: TextInputType.text,
                            controller:
                                _textFieldControllerAnniversaryBirthdate,
                            decoration: InputDecoration(
                              hintText: 'Anniversary Date',
                              labelText: 'Anniversary Date',
                              filled: true,
                              fillColor: formElementColor,
                              prefixIcon: Icon(
                                Icons.date_range,
                                color: redColor,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0),
                                borderSide: new BorderSide(
                                  color: formElementColor,
                                  width: 2.0,
                                ),
                              ),
                              contentPadding:
                                  EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                            ),
                            onSaved: (String val) {
                              _annivarsaryDate = val;
                            },
                          ),
                        ),
                      ),
                      // ),
                      SizedBox(height: 20.0),
                      SizedBox(
                        height: 50.0, // match_parent
                        child: RaisedButton(
                          onPressed: _validateInputs,
                          // padding: EdgeInsets.all(10),
                          color: redColor,
                          child: Text(
                            "UPDATE PROFILE",
                            style: TextStyle(
                              color: whiteColor,
                              fontSize: 22.0,
                            ),
                          ),
                        ),
                      ),

                      SizedBox(height: 20.0),
                    ],
                  )
                : new Stack(
                    children: [
                      new Opacity(
                        opacity: 0.3,
                        child: const ModalBarrier(
                            dismissible: false, color: Colors.grey),
                      ),
                      new Center(
                        child: new CircularProgressIndicator(
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(redColor),
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  void _validateInputs() {
    final _form = _formKey.currentState;
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_formKey.currentState.validate()) {
      // If the form is valid, we want to show a Snackbar
      setState(() => _isLoading = true);
      _form.save();
      doUpdateProfile();
    } else {
      _autoValidate = true;
    }
  }

  void doUpdateProfile() async {
    RestData api = new RestData();
    try {
      var request = await api.updateProfile(
          user.id.toString(),
          _name.toString(),
          _companyName.toString(),
          _location.toString(),
          _birthDate.toString(),
          _annivarsaryDate.toString(),
          _gstNO.toString());

      if (request["Response"] == true) {
        User tempuser = new User.fromJson(request["Result"]);

        var db = new DatabaseHelper();
        await db.updateUser(tempuser);
        // User user = await db.getData();
        setState(() {
          _isLoading = false;
          user = tempuser;
        });

        _showSnackBar("Profile Updated Successfully!!!");

        Future.delayed(Duration(seconds: 1), () {
          Navigator.pop(context, user);
        });
      } else {
        _showSnackBar(request["Message"].toString());

        print("view");
      }
      setState(() => _isLoading = false);
    } catch (error) {
      _showSnackBar(error.toString());
    }
  }

  Future showAlert(BuildContext context, String title) async {
    await showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Text(title),
              actions: <Widget>[
                new FlatButton(
                  child: new Text("Dismiss"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            )).then((val) {
      Navigator.pop(context, user);
    });
  }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  Future _selectBirthDate() async {
    DateTime selectedDate = DateTime.now();
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateFormat("MM/dd/yyyy", "en_US").parse(
            (_birthDate == "" || _birthDate == null)
                ? "01/01/1990"
                : _birthDate),
        firstDate: new DateTime(1950),
        lastDate: new DateTime(2019),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(

                // primarySwatch : redColor,//OK/Cancel button text color
                primaryColor: redColor, //Head background
                accentColor: redColor //selection color
                //dialogBackgroundColor: Colors.white,//Background color
                ),
            child: child,
          );
        });
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        _textFieldControllerBirthdate = new TextEditingController(
            text: new DateFormat("MM/dd/yyyy", "en_US").format(picked));
        _birthDate = new DateFormat("MM/dd/yyyy", "en_US").format(picked);
      });
  }

  Future _selectAnniversaryDate() async {
    DateTime selectedDate = DateTime.now();
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateFormat("MM/dd/yyyy", "en_US").parse(
          (_annivarsaryDate == "" || _annivarsaryDate == null)
              ? "01/01/2010"
              : _annivarsaryDate),
      firstDate: new DateTime(1950),
      lastDate: new DateTime.now(),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(

              // primarySwatch : redColor,//OK/Cancel button text color
              primaryColor: redColor, //Head background
              accentColor: redColor //selection color
              //dialogBackgroundColor: Colors.white,//Background color
              ),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        _textFieldControllerAnniversaryBirthdate = new TextEditingController(
            text: new DateFormat("MM/dd/yyyy", "en_US").format(picked));
        _annivarsaryDate = new DateFormat("MM/dd/yyyy", "en_US").format(picked);
      });
  }
}
