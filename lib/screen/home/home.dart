import 'dart:io';

import 'package:flutter/material.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';
import 'package:tectronics/screen/drawer.dart';
import 'package:tectronics/screen/product/productcategory.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../const.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'notifications.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  var db = new DatabaseHelper();
  User _user;

  List listDataAPI = [];

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  @override
  void initState() {
    super.initState();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message $message');
        _showItemDialog(message);
        // scaffoldKey.currentState.showSnackBar(SnackBar(
        //   content: Text('Assign a GlobalKey to the Scaffold'),
        //   duration: Duration(seconds: 3),
        // ));
      },
      onResume: (Map<String, dynamic> message) {
        print('on resume $message');
        _navigateToItemDetail(message);
        // Navigator.of(context).push(message['/notification_list']);
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on launch $message');
        _navigateToItemDetail(message);
        // Navigator.of(context).push(message['/notification_list']);
      },
    );
    _firebaseMessaging.getToken().then((token) {
      // _token = token;
      print("Device TOken:" + token);
    });

    getAllCategory();
    _getUserData();
  }

  _getUserData() async {
    var db = new DatabaseHelper();
    User user = await db.getData();
    print(user.name);
    setState(() {
      _user = user;
    });
  }

  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    Size screenSize = MediaQuery.of(context).size;
    EdgeInsets checkPadding = MediaQuery.of(context).padding;
    return new Scaffold(
        backgroundColor: whiteColor,
        drawer: DrawerOnly(user: _user),
        appBar: AppBar(
          // leading: IconButton(
          //   icon: Icon(Icons.arrow_back),
          //   onPressed: () {
          //     Navigator.pop(context);
          //   },
          // ),
          centerTitle: true,
          elevation: 10.0,
          backgroundColor: redColor,
          iconTheme: IconThemeData(
            color: Colors.white,
            //change your color here
          ),
          title: Text(
            "HOME",
            style: TextStyle(
              color: whiteColor,
            ),
          ),
          actions: <Widget>[
            new IconButton(
              icon: Icon(
                Icons.notifications,
                color: whiteColor,
                size: 30,
              ),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => NotificationsPage(user: _user)));
              },
            )
          ],
        ),
        key: scaffoldKey,
        body: new SafeArea(
          top: true,
          bottom: true,
          // minimum: const EdgeInsets.all(16.0),
          child: new Column(children: <Widget>[
            Stack(children: <Widget>[
              _buildCoverImage(screenSize),
              _buildTopImage(screenSize),
            ]),
            ConstrainedBox(
                constraints: BoxConstraints(
                    maxHeight: screenSize.height -
                        screenSize.height / 2.75 -
                        65 -
                        checkPadding.top -
                        checkPadding.bottom),
                child: !_isLoading
                    ? new GridView.builder(
                        // physics: new NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: listDataAPI.length,
                        padding: const EdgeInsets.all(4.0),
                        gridDelegate:
                            new SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount:
                                    (orientation == Orientation.portrait)
                                        ? 2
                                        : 3),
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                              color: backgroundGrey,
                              // width: 160.0,
                              // color: Color(0xee0290).withOpacity(1.0),// colorData[index],
                              margin: new EdgeInsets.all(8.0),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => ProductCategoryPage(
                                          categoryDetails:
                                              listDataAPI[index])));
                                  print(listDataAPI[index]);
                                },
                                child: new GridTile(
                                    footer: new Center(
                                      child: new Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 8.0, left: 2.0, right: 2.0),
                                        child: new AutoSizeText(
                                          listDataAPI[index]["Name"].toString(),
                                          style: new TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                            color: redColor,
                                          ),
                                          maxLines: 1,
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),

                                    // new Text("data[index]['name']"),
                                    child: new Center(
                                      child: new Padding(
                                          padding: const EdgeInsets.only(
                                              left: 35.0,
                                              bottom: 45.0,
                                              right: 35.0,
                                              top: 25.0),
                                          child: Stack(
                                            children: <Widget>[
                                              Center(
                                                child:
                                                    new CircularProgressIndicator(
                                                  valueColor:
                                                      new AlwaysStoppedAnimation<
                                                          Color>(redColor),
                                                ),
                                              ),
                                              Center(
                                                child:
                                                    FadeInImage.memoryNetwork(
                                                  placeholder:
                                                      kTransparentImage,
                                                  image: listDataAPI[index]
                                                          ["productDetail"][0]
                                                      ["ThumbImage"],
                                                ),
                                              ),
                                              // Center(
                                              //   child: new Image.asset(
                                              //     'assets/products/${listDataAPI[index]["Name"]}.png',
                                              //   ),
                                              // ),
                                            ],
                                          )),
                                    )),

                                // new GridTile(
                                //   child: new Center(
                                //       child: Column(
                                //     crossAxisAlignment:
                                //         CrossAxisAlignment.start,
                                //     children: <Widget>[
                                //       new Padding(
                                //         padding: const EdgeInsets.only(
                                //             bottom: 18.0, top: 25.0),
                                //         child: Center(
                                //             child: new Image.asset(
                                //           'assets/products/${listDataAPI[index]["Name"]}.png',
                                //           fit: BoxFit.fitHeight,
                                //           height: screenSize.width / 5,
                                //         )),
                                //       ),
                                //       Align(
                                //         alignment: Alignment.bottomCenter,
                                //         child: new Padding(
                                //             padding: const EdgeInsets.only(
                                //               bottom: 10.0,
                                //             ),
                                //             child: Center(
                                //               child: new Text(
                                //                 listDataAPI[index]["Name"],
                                //                 style: new TextStyle(
                                //                   fontWeight: FontWeight.bold,
                                //                   // fontSize: 17.0,
                                //                   color: redColor,
                                //                 ),
                                //                 maxLines: 1,
                                //                 textAlign: TextAlign.center,
                                //               ),
                                //             )),
                                //       )
                                //     ],
                                //   )),
                                // ),
                              ));
                        },
                      )
                    : new Stack(
                        children: [
                          new Opacity(
                            opacity: 0.3,
                            child: const ModalBarrier(
                                dismissible: false, color: Colors.grey),
                          ),
                          new Center(
                            child: new CircularProgressIndicator(
                              valueColor:
                                  new AlwaysStoppedAnimation<Color>(redColor),
                            ),
                          ),
                        ],
                        // )),
                      )),
            SizedBox(height: 2.0),
          ]),
          // ]),
        ));

    // );
  }

  Widget _buildCoverImage(Size screenSize) {
    return Container(
      height: screenSize.height / 2.75,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/home/banner-bg.png'),
              fit: BoxFit.fill)),
    );
  }

  Widget _buildTopImage(Size screenSize) {
    return Container(
      height: screenSize.height / 2.75,
      child: Center(
        child: Image.asset(
          'assets/home/banner-circle.png',
          fit: BoxFit.fitHeight,
          height: screenSize.height / 3.25,
        ),
      ),
      // decoration: BoxDecoration(
      //     image: DecorationImage(
      //         image: AssetImage('assets/home/banner-circle.png'),
      //         fit: BoxFit.fitHeight)),
    );
  }

  void getAllCategory() async {
    RestData api = new RestData();
    setState(() => _isLoading = true);

    try {
      setState(() => _isLoading = true);
      var request = await api.getAllProducts();
      print(request);
      if (request["Response"] == true) {
        setState(() {
          _isLoading = false;

          List tempData = request["Result"];
          listDataAPI.clear();
          for (int x = 0; x < tempData.length; x++) {
            var categoryData = tempData[x];
            // print("categoryData" + categoryData);
            List productsData = tempData[x]["productDetail"];
            if (productsData.length > 0) {
              listDataAPI.add(categoryData);
            }
          }
        });
      } else {
        _showSnackBar(request["Message"].toString());
        print("view");
      }
      setState(() => _isLoading = false);
    } catch (error) {
      _showSnackBar(error.toString());
    }
  }

  void _showSnackBar(String text) {
    setState(() => _isLoading = false);

    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  void _navigateToItemDetail(Map<String, dynamic> message) {
    final newRouteName = "/notification_list";
    bool isNewRouteSameAsCurrent = false;

    Navigator.popUntil(context, (route) {
      if (route.settings.name == newRouteName) {
        isNewRouteSameAsCurrent = true;
      }
      return true;
    });

    if (!isNewRouteSameAsCurrent) {
      Navigator.pushNamed(context, newRouteName);
    } else {
      // Navigator.pop(context);
      // Navigator.pushNamed(context, newRouteName);
    }
    // Navigator.of(_scaffoldKey.currentContext).pushNamed('/notification_list');
  }

  void _showItemDialog(Map<String, dynamic> message) {
    showDialog<bool>(
      context: context,
      builder: (_) => _buildDialog(context, message),
    ).then((bool shouldNavigate) {
      if (shouldNavigate == true) {
        _navigateToItemDetail(message);
      }
    });
  }

  Widget _buildDialog(BuildContext context, Map<String, dynamic> item) {
    return new AlertDialog(
        // title: new Text("You Have New Notification"),
        content: new Text("You Have New Notification"),
        actions: <Widget>[
          new FlatButton(
            child: const Text('CLOSE'),
            onPressed: () {
              Navigator.pop(context, false);
            },
          ),
          new FlatButton(
            child: const Text('SHOW'),
            onPressed: () {
              Navigator.pop(context, true);
            },
          ),
        ]);
  }
}
