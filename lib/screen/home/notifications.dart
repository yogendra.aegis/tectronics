import 'package:flutter/material.dart';
import 'package:tectronics/data/database_helper.dart';
import 'package:tectronics/data/rest_data.dart';
import 'package:tectronics/modal/user.dart';
import 'package:intl/intl.dart';

import '../../const.dart';

class NotificationsPage extends StatefulWidget {
  NotificationsPage({Key key, this.user}) : super(key: key);
  final User user;
  @override
  NotificationsState createState() => new NotificationsState(user);
}

class NotificationsState extends State<NotificationsPage> {
  NotificationsState(this._user);
  User _user;

  var db = new DatabaseHelper();
  List listDataAPI = [];
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  List notificationListData = [];

  bool _isLoading = false;

  List<dynamic> _dataProductModelDropDown = []; // Option 2
  dynamic _selectedDataProductModel;

  @override
  void initState() {
    super.initState();
    _getUserData();
  }

  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          elevation: 10.0,
          backgroundColor: redColor,
          iconTheme: IconThemeData(
            color: Colors.white,
            //change your color here
          ),
          title: Text(
            "Notifications",
            style: TextStyle(
              color: whiteColor,
            ),
          ),
        ),
        key: scaffoldKey,
        body: new Container(
          margin: EdgeInsets.symmetric(vertical: 10.0),
          // height: 200.0,
          child: !_isLoading
              ? ListView.builder(
                  itemBuilder: (context, index) {
                    return Card(
                      elevation: 5.0,
                      color: (notificationListData[index]["IsRead"] == false)
                          ? Color(0xff555555)
                          : Color(0xffEEEEEE),
                      // margin: new EdgeInsets.symmetric(
                      //     horizontal: 5.0, vertical: 6.0),
                      child: ListTile(
                          // contentPadding: EdgeInsets.symmetric(
                          //     horizontal: 20.0, vertical: 10.0),

                          title: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  "${notificationListData[index]["Title"]}",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.0),
                                ),
                              ),
                              Text(
                                readTimestamp(int.parse(
                                    notificationListData[index]["Date"])),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                          subtitle: Text(
                            "${notificationListData[index]["Description"]}",
                            textAlign: TextAlign.justify,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.normal),
                          ),
                          // trailing: Text(
                          //   readTimestamp(
                          //       int.parse(notificationListData[index]["Date"])),
                          //   style: TextStyle(
                          //       color: Colors.black,
                          //       fontWeight: FontWeight.normal),
                          // ),
                          // Icon(Icons.keyboard_arrow_right,
                          //     color: Colors.black, size: 30.0)

                          onTap: () {
                            showDialod(
                                context,
                                notificationListData[index]["Title"],
                                notificationListData[index]["Description"]);
                          }),
                    );
                  },
                  physics: ClampingScrollPhysics(),
                  itemCount: notificationListData.length,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true, // this is a hardcoded value
                )
              : new Stack(
                  children: [
                    new Opacity(
                      opacity: 0.3,
                      child: const ModalBarrier(
                          dismissible: false, color: Colors.grey),
                    ),
                    new Center(
                      child: new CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(redColor),
                      ),
                    ),
                  ],
                ),
        ));
  }

  Future _showSnackBar(String text) async {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  _getUserData() async {
    var db = new DatabaseHelper();
    User user = await db.getData();
    setState(() {
      _user = user;
      _getNotificationData();
    });
  }

  void _getNotificationData() async {
    RestData api = new RestData();
    setState(() => _isLoading = true);

    try {
      setState(() => _isLoading = true);
      var request = await api.getAllNotification(_user.id.toString());
      print(request);
      if (request["Response"] == true) {
        setState(() {
          _isLoading = false;
          List tempData = request["Result"];
          notificationListData.clear();
          notificationListData = (tempData == null) ? [] : tempData;
        });
      } else {
        _showSnackBar(request["Message"].toString());
        print("view");
      }
      setState(() => _isLoading = false);
    } catch (error) {
      _showSnackBar(error.toString());
    }
  }

  String readTimestamp(int timestamp) {
    var now = new DateTime.now();
    var format = new DateFormat('HH:mm a');
    var date = new DateTime.fromMillisecondsSinceEpoch(timestamp, isUtc: true);
    print("Date Old: " + date.toString());
    print("Date Now: " + now.toString());
    var diff = now.difference(date);
    var time = '';

    if (diff.inSeconds <= 0 ||
        diff.inSeconds > 0 && diff.inMinutes == 0 ||
        diff.inMinutes > 0 && diff.inHours == 0 ||
        diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date);
    } else {
      time = new DateFormat('dd MMM').format(
          DateTime.fromMillisecondsSinceEpoch((timestamp), isUtc: true));
      // time = format1.format(date);

    }

    // else if (diff.inDays > 0 && diff.inDays < 7) {
    //   if (diff.inDays == 1) {
    //     time = diff.inDays.toString() + ' DAY AGO';
    //   } else {
    //     time = diff.inDays.toString() + ' DAYS AGO';
    //   }
    // } else {
    //   if (diff.inDays == 7) {
    //     time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
    //   } else {
    //     time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
    //   }
    // }
    return time;
  }

  Future<bool> showDialod(BuildContext context, String title, String subTitle) {
    // return showGeneralDialog(
    //     context: context,
    //     barrierDismissible: true,
    //     barrierLabel:
    //         MaterialLocalizations.of(context).modalBarrierDismissLabel,
    //     barrierColor: Colors.black45,
    //     transitionDuration: const Duration(milliseconds: 200),
    //     pageBuilder: (BuildContext buildContext, Animation animation,
    //         Animation secondaryAnimation) {
    //       return Center(
    //         child: Container(
    //           // width: MediaQuery.of(context).size.width - 20,
    //           // height: MediaQuery.of(context).size.height -  120,
    //           padding: EdgeInsets.all(30),
    //           color: Colors.white,
    //           child: Column(
    //             children: [
    //               new Text(
    //                 title,
    //                 style: TextStyle(
    //                   fontWeight: FontWeight.bold,
    //                   fontSize: 22.0,
    //                 ),
    //               ),
    //               SingleChildScrollView(
    //                 child: ListBody(children: <Widget>[
    //                   new Text(
    //                     subTitle,
    //                     textAlign: TextAlign.justify,
    //                     style: TextStyle(
    //                       fontWeight: FontWeight.normal,
    //                       fontSize: 18.0,
    //                     ),
    //                   ),
    //                 ]),
    //               ),
    //               RaisedButton(
    //                 onPressed: () {
    //                   Navigator.of(context).pop();
    //                 },
    //                 child: Text(
    //                   "Save",
    //                   style: TextStyle(color: Colors.white),
    //                 ),
    //                 color: const Color(0xFF1BC0C5),
    //               )
    //             ],
    //           ),
    //         ),
    //       );
    //     });

    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0)), //this right here

            title: new Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 22.0,
              ),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  new Text(
                    subTitle,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text(
                  'Dismiss',
                  
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    color: redColor
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              )
            ],
          );
        });
  }
}
