class User {
  int _id;
  String _name;
  String _companyName;
  String _location;
  String _email;
  String _mobileNumber;
  String _birthDate;
  String _anniversaryDate;
  String _oTP;
  String _gstNo;
  String _isVerify;

  User(
      this._id,
      this._name,
      this._companyName,
      this._location,
      this._email,
      this._mobileNumber,
      this._birthDate,
      this._anniversaryDate,
      this._oTP,
      this._gstNo,
      this._isVerify);

  User.fromJson(Map<String, dynamic> obj) {
    this._id = obj["Id"];
    this._name = obj["Name"].toString();
    this._companyName = obj["CompanyName"].toString();
    this._location = obj["Location"].toString();
    this._email = obj["Email"].toString();
    this._mobileNumber = obj["MobileNumber"].toString();
    this._birthDate = obj["BirthDate"].toString();
    this._anniversaryDate = obj["AnniversaryDate"].toString();
    this._oTP = obj["OTP"].toString();
    this._gstNo = obj["GSTNo"].toString();
    this._isVerify = obj["IsVerify"].toString();
  }

  int get id => _id;
  String get name => _name;
  String get companyName => _companyName;
  String get location => _location;
  String get email => _email;
  String get mobileNumber => _mobileNumber;
  String get birthDate => _birthDate;
  String get anniversaryDate => _anniversaryDate;
  String get oTP => _oTP;
  String get gstNo => _gstNo;
  String get isVerify => _isVerify;

  set id(int value) => _id = value;
  set name(String value) => _name = value;
  set companyName(String value) => _companyName = value;
  set location(String value) => _location = value;
  set email(String value) => _email = value;
  set mobileNumber(String value) => _mobileNumber = value;
  set birthDate(String value) => _birthDate = value;
  set anniversaryDate(String value) => _anniversaryDate = value;
  set oTP(String value) => _oTP = value;
  set gstNo(String value) => _gstNo = value;
  set isVerify(String value) => _isVerify = value;

  Map<String, dynamic> toJson() {
    var map = new Map<String, dynamic>();

    map["Id"] = _id;
    map["Name"] = _name;
    map["CompanyName"] = _companyName;
    map["Location"] = _location;
    map["Email"] = _email;
    map["MobileNumber"] = _mobileNumber;
    map["BirthDate"] = _birthDate;
    map["AnniversaryDate"] = _anniversaryDate;
    map["OTP"] = _oTP;
    map["GSTNo"] = _gstNo;
    map["IsVerify"] = _isVerify;

    return map;
  }
}
