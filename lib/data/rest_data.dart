import 'package:tectronics/modal/user.dart';
import 'package:tectronics/util/network_util.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart' as crypto;

class RestData {
  // /APIAuth/Login
  NetworkUtil _netUtil = new NetworkUtil();
  static final IMAGE_URL = "https://tectronicsindia.com";
  static final BASE_URL = "https://api.tectronicsindia.com/api/";
  static final LOGIN_URL = BASE_URL + "APIAuth/SignIn";
  static final REGISTRATION_URL = BASE_URL + "APIAuth/SignUp";
  static final VERIFYOTP = BASE_URL + "APIAuth/VerifyOTP";
  static final FORGOT_PASSWORD = BASE_URL + "APIAuth/ForgotPassword";
  static final RESET_PASSWORD = BASE_URL + "APIAuth/ResetPassword";
  static final RESEND_OTP = BASE_URL + "APIAuth/Resend";
  static final CONTACT_US = BASE_URL + "APIAuth/SaveContactUs";
  static final UPDATE_PROFILE = BASE_URL + "APIAuth/UpdateProfile";
  static final CHANGE_PASSWORD = BASE_URL + "APIAuth/ChangePassword";
  static final ALL_NOTIFICATION = BASE_URL + "APIAuth/GetAllNotifications";
  static final DOWNLOAD = BASE_URL + "APIAuth/Downloads";

  generateMd5(String data) {
    var content = new Utf8Encoder().convert(data);
    var md5 = crypto.md5;
    var digest = md5.convert(content);
    return hex.encode(digest.bytes);
  }

  Future<dynamic> login(String username, String password, String token) {
    return _netUtil
        .post(LOGIN_URL, body: {"Email": username, "Password": generateMd5(password), "Token": token}).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> registration(String name, String companyName, String location, String email, String mobileNumber,
      String birthDate, String anniversaryDate, String password, String gstNO) {
    return _netUtil.post(REGISTRATION_URL, body: {
      "Name": name,
      "CompanyName": companyName,
      "Location": location,
      "Email": email,
      "MobileNumber": mobileNumber,
      "BirthDate": birthDate,
      "AnniversaryDate": anniversaryDate,
      "Password": generateMd5(password),
      "GSTNo": gstNO
    }).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> forgotpassword(String username) {
    String pattternEmail = r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    String pattternMobile = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExpE = new RegExp(pattternEmail);
    RegExp regExpM = new RegExp(pattternMobile);
    if (regExpE.hasMatch(username)) {
      print("Email:" + username);
      return _netUtil.post(FORGOT_PASSWORD, body: {"Email": username}).then((dynamic res) {
        print(res.toString());
        return res;
      });
    } else {
      print("MobileNumber:" + username);
      return _netUtil.post(FORGOT_PASSWORD, body: {"MobileNumber": username}).then((dynamic res) {
        print(res.toString());
        return res;
      });
    }
  }

  Future<dynamic> resetPassword(String userId, String password) {
    return _netUtil.post(RESET_PASSWORD, body: {"Id": userId, "NewPassword": generateMd5(password)}).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> verifyOTP(String userId, String otp) {
    return _netUtil.post(VERIFYOTP, body: {"UserId": userId, "Otp": otp}).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> resendOTP(String userId) {
    return _netUtil.post(RESEND_OTP, body: {"UserId": userId}).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> contactUS(String userId, String name, String email, String phone, String description, String type) {
    print(userId + name + email + phone + description + type);
    return _netUtil.post(CONTACT_US, body: {
      "UserId": userId,
      "Name": name,
      "Email": email,
      "Phone": phone,
      "Description": description,
      "Type": type
    }).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> updateProfile(
      String userId, String name, String companyName, String location, String birthDate, String anniversaryDate, String gstNO) {
    return _netUtil.post(UPDATE_PROFILE, body: {
      "Id": userId,
      "Name": name,
      "CompanyName": companyName,
      "Location": location,
      "BirthDate": birthDate,
      "AnniversaryDate": anniversaryDate,
      "GSTNo": gstNO
    }).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> changePassword(String userId, String oldPassword, String newPassword) {
    return _netUtil.post(CHANGE_PASSWORD, body: {
      "Id": userId,
      "OldPassword": generateMd5(oldPassword),
      "NewPassword": generateMd5(newPassword)
    }).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> getAllNotification(String userId) {
    return _netUtil.post(ALL_NOTIFICATION, body: {
      "Id": userId,
    }).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> getAllCategories() {
    return _netUtil.post("https://tectronicsindia.com/api/Product/GetAllCategories", body: {}).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> getAllProductsByCategory(String categoryID) {
    return _netUtil.post("https://tectronicsindia.com/api/Product/GetAllProductsByCategory",
        body: {"CategoryId": categoryID}).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> getAllProducts() {
    return _netUtil.post("https://tectronicsindia.com/api/Product/GetAllProductDetails", body: {}).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> getGalleryDetails() {
    return _netUtil.post("https://tectronicsindia.com/api/Product/GetGalleryDetails", body: {}).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> generatePDFFile(String productId, String modelName) {
    return _netUtil.post("https://tectronicsindia.com/api/Product/GetProductPDF",
        body: {"ProductId": productId, "ModelName": modelName}).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }

  Future<dynamic> downloads() {
    return _netUtil.post(DOWNLOAD, body: {}).then((dynamic res) {
      print(res.toString());
      return res;
    });
  }
}
