import 'package:tectronics/modal/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io' as io;
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database _db;
  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }

    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();
  initDb() async {
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, "tectronics.db");

    print("Path:" + path);

    var ourDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return ourDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE User(Id INTEGER, Name TEXT, CompanyName TEXT, Location TEXT, Email TEXT, MobileNumber TEXT, BirthDate TEXT, AnniversaryDate TEXT, OTP TEXT, GSTNo TEXT, IsVerify TEXT)");
    print("User Table is Created");
  }

  void _onDeleteTable(Database db, int version) async {
    await db.execute("DROP TABLE IF EXISTS User");

    print("User Table is Deleted");
  }

// insertion
  Future<int> saveUser(User user) async {
    var dbClient = await db;
    int res = await dbClient.insert("User", user.toJson());
    print("res : " + res.toString());
    return res;
  }

  Future saveUserForce(User user) async {
    var dbClient = await db;
    await dbClient.execute("DROP TABLE IF EXISTS User");
    print("User Table is Deleted");
    await dbClient.execute(
        "CREATE TABLE User(Id INTEGER, Name TEXT, CompanyName TEXT, Location TEXT, Email TEXT, MobileNumber TEXT, BirthDate TEXT, AnniversaryDate TEXT, OTP TEXT, GSTNo TEXT, IsVerify TEXT)");
    print("User Table is Created");
    await dbClient.insert("User", user.toJson());
  }

  // delete
  Future<int> deleteUser(User user) async {
    var dbClient = await db;
    int res = await dbClient.delete("User");
    return res;
  }

  Future<int> updateUser(User user) async {
    var dbClient = await db;
    int res = await dbClient.update("User", user.toJson());
    return res;
  }

  Future<bool> isLoggedIn() async {
    var dbClient = await db;
    var res = await dbClient.query("User");
    return res.length > 0 ? true : false;
  }

  Future<User> getData() async {
    var dbClient = await db;
    var res = await dbClient.query("User");
    return res.length > 0 ? User.fromJson(res.first) : Null;
  }
}
