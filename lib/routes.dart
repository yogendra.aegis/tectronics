import 'package:flutter/material.dart';
import './screen/login/login.dart';
// import './screen/login/registration.dart';
// import './screen/home/home.dart';
// import './screen/Profile/personal_information.dart';
// import './screen/Notification/notification_list.dart';

final routes = {
  '/login': (BuildContext context) => new LoginPage(),
  // '/forgotpassword': (BuildContext context) => new ForgotPasswordPage(),
  // '/home': (BuildContext context) => new HomePage(),
  // '/personal_information': (BuildContext context) => new PersonalInformationPage(),
  // '/notification_list': (BuildContext context) => new NotificationPage(),
  '/': (BuildContext context) => new LoginPage(),
};
