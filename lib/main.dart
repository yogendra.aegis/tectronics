import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
// import 'package:why_not_gym/routes.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:tectronics/screen/home/notifications.dart';
import './screen/login/login.dart';
import './screen/login/otpverify.dart';
import './screen/login/registration.dart';
import './screen/login/forgotpassword.dart';
import './screen/login/resetpassword.dart';
import './screen/home/home.dart';
import './auth.dart';
// import 'package:permission_handler/permission_handler.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize();

  // Map<PermissionGroup, PermissionStatus> permissions =
  //     await PermissionHandler().requestPermissions([PermissionGroup.storage]);
  // PermissionStatus permission =
  //     await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      FlutterStatusbarcolor.setStatusBarColor(Colors.black);
      FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    }
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return MaterialApp(
      title: 'Tectronics',
      theme: ThemeData(primarySwatch: Colors.blue, fontFamily: 'segoeui'),
      initialRoute: '/',
      // home: SplashScreenPage(),
      routes: {
        '/login': (BuildContext context) => new LoginPage(),
        '/register': (BuildContext context) => new RegistrationPage(),
        '/otpverify': (BuildContext context) => new OtpPage(),
        '/forgotpassword': (BuildContext context) => new ForgotPasswordPage(),
        '/resetpassword': (BuildContext context) => new ResetPasswordPage(),
        '/home': (BuildContext context) => new HomePage(),
        '/notification_list': (BuildContext context) => new NotificationsPage(),
        '/': (BuildContext context) => new SplashScreenPage(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreenPage>
    implements AuthStateListener {
  _SplashScreenState() {
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  bool _isLoading = true;
  @override
  void onAuthStateChanged(AuthState state) {
    Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        if (state == AuthState.LOGGED_IN)
          Navigator.of(context).pushReplacementNamed("/home");
        else if (state == AuthState.LOGGED_OUT)
          Navigator.of(context).pushReplacementNamed("/login");
      });
    });
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 200), () {
      setState(() {
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Stack(children: <Widget>[
      Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/my_splash.png'), fit: BoxFit.fill)),
      ),
      !_isLoading
          ? Center(
              child: Container(
                width: screenSize.width / 1.50,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/logo1.png'),
                        fit: BoxFit.fitWidth)),
              ),
            )
          : Container()
    ]);
  }
}
