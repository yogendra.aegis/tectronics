import 'package:flutter/material.dart';

import 'const.dart';

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key, String mytext) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        new CircularProgressIndicator(
                            valueColor:
                                new AlwaysStoppedAnimation<Color>(redColor)),
                        SizedBox(
                          height: 20,
                        ),
                        Text("Please wait...",
                            style: TextStyle(color: Colors.redAccent)),
                        SizedBox(
                          height: 10,
                        ),
                        Text(mytext, style: TextStyle(color: Colors.redAccent))
                      ]),
                    )
                  ]));
        });
  }
}
