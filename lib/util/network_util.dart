import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class NetworkUtil {
  // next three lines makes this class a Singleton
  static NetworkUtil _instance = new NetworkUtil.internal();
  NetworkUtil.internal();
  factory NetworkUtil() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();

  Future<dynamic> get(String url) {
    return http.get(url).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
      return _decoder.convert(res);
    });
  }

  Future<dynamic> post(String url, {Map headers, body, encoding}) {
    return http
        .post('$url',
            body: jsonEncode(body),
            encoding: encoding,
            headers: {"Content-Type": "application/json"})
        .timeout(const Duration(seconds: 60),
            onTimeout: () => throw new Exception(
                "Error while fetching data. Please check your network connection"))
        .then((http.Response response) {
          print("URL:" + url.toString());
          print("body:" + body.toString());
          print("Response:" + response.toString());

          final String res = response.body;
          final int statusCode = response.statusCode;
          Map<String, dynamic> responseData = _decoder.convert(res);

          if (statusCode == 200) {
            return responseData;
          } else {
            return ("Error while fetching data");
          }
          // throw new Exception("Error while fetching data");
        });
  }
}
