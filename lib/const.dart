import 'dart:ui';

import 'package:flutter/material.dart';

final globalScaffoldKey = new GlobalKey<ScaffoldState>();


final redColor = Color(0xffED1C24);
final greyColor = Color(0xfff5f5f5);
final menuBG = Color(0xff333333);
final formElementColor = Color(0xffeeeeee);
final textColor = Color(0xff333333);
final selectModelColor = Color(0xffdddddd);
final backgroundGrey = Color(0xfff5f5f5);

final backgroundSplash = Color(0xffe0e0e0);

final whiteColor = Color(0xffffffff);